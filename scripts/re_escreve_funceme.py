#!/usr/bin/env python3.6

import os
import xarray as xr

from writenc_mon import writenc_mon

script_dir = os.path.dirname(os.path.realpath(__file__))

# 0.25

arq_entrada = f'{script_dir}/../data/acumulado_mensal' \
               '/funceme_pr_mon_total_0.25x0.25_196101-201812.old.nc'

with xr.open_dataset(arq_entrada, decode_times=False) as dset:
    print(dset)
    pr = dset['pr'].values
    lat = dset['latitude'].values
    lon = dset['longitude'].values
    len_time = len(dset['time'].values)

arq_saida = f'{script_dir}/../data/acumulado_mensal' \
             '/funceme_pr_mon_total_0.25x0.25_196101-201812.nc'

writenc_mon(pr, lat, lon, fname=arq_saida, timeunits='days',
            timecalendar='360_day', ntimes=len_time,
            year=1961, month=1, day=15)

print(arq_saida)

raise SystemExit

# 0.50

arq_entrada = f'{script_dir}/../data/acumulado_mensal' \
               '/funceme_pr_mon_total_0.5x0.5_196101-201812.old.nc'

with xr.open_dataset(arq_entrada, decode_times=False) as dset:
    print(dset)
    pr = dset['pr'].values
    lat = dset['latitude'].values
    lon = dset['longitude'].values
    len_time = len(dset['time'].values)

arq_saida = f'{script_dir}/../data/acumulado_mensal' \
             '/funceme_pr_mon_total_0.5x0.5_196101-201812.nc'

writenc_mon(pr, lat, lon, fname=arq_saida, timeunits='days',
            timecalendar='360_day', ntimes=len_time,
            year=1961, month=1, day=15)

print(arq_saida)
