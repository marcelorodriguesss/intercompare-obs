#!/usr/bin/env python3.6

'''Este script lê as bases observadas e padroniza os dados, 
as latitudes, longitudes, acumulados mensais e as retorna 
no formato xarray em um dicionário'''

import os
import xarray as xr
import pandas as pd
import numpy as np
# import pickle
from mpl_toolkits.basemap import shiftgrid

from writenc_mon import writenc_mon

def load_obs_data():

    script_dir = os.path.dirname(os.path.realpath(__file__))

    ######## cmap ######## 1

    arq_nc = f'{script_dir}/../data/br/' \
        f'cmap_precip_mon_mean_2.5x2.5_198101-201112.nc'

    print(f'input:  {arq_nc}')
    
    with xr.open_dataset(arq_nc) as dset:

        # xarray inverte dado e latitude
        cmap = dset['precip'][:, ::-1, :]
    
        # acumulado mensal
        cmap = cmap * cmap.time.dt.daysinmonth  
        
        # substitui np.nan por -999.0
        cmap.values[np.isnan(cmap.values)] = -999.0

        # ttime = cmap.coords['time'].values

        lons = cmap.coords['lon'].values

        new_lats = cmap.coords['lat'].values

        # shift na longitude de 0:360 para -180:180
        new_cmap, new_lons = shiftgrid(lons[0], cmap.values, 
                                       lons, start=False)

        fout = f'{script_dir}/../data/br-accum-mensal/' \
            f'cmap_precip_mon_total_2.5x2.5_198101-201112.nc'

        writenc_mon(new_cmap, new_lats, new_lons, fname=fout)

        print(f'output: {fout}')

        # novo xarray
        # cmap = xr.DataArray(new_cmap, coords=[ttime, new_lats, new_lons], 
        #                     dims=['time', 'lat', 'lon']) 

        # print(cmap.coords['lon'].values)

        # print(cmap.coords['lat'].values)

        # print(cmap.values.shape)

        del lons, new_lats, new_lons  # , ttime


    ######## precl ######## 2

    arq_nc = f'{script_dir}/../data/br/' \
        f'precl_precip_mon_mean_0.5x0.5_198101-201112.nc'

    print(f'input:  {arq_nc}')

    with xr.open_dataset(arq_nc) as dset:

        precl = dset['precip'][:, ::-1, :]

        precl = precl * precl.time.dt.daysinmonth

        precl.values[np.isnan(precl.values)] = -999.0

        # ttime = precl.coords['time'].values

        lons = precl.coords['lon'].values

        new_lats = precl.coords['lat'].values

        new_precl, new_lons = shiftgrid(lons[0], precl.values, 
                              lons, start=False)

        fout = f'{script_dir}/../data/br-accum-mensal/' \
            f'precl_precip_mon_total_0.5x0.5_198101-201112.nc'

        writenc_mon(new_precl, new_lats, new_lons, fname=fout, ntimes=new_precl.shape[0])

        print(f'output: {fout}')

        # precl = xr.DataArray(new_precl, coords=[ttime, new_lats, new_lons], 
        #                      dims=['time', 'lat', 'lon'])

        # print(precl.coords['lon'].values)

        # print(precl.coords['lat'].values)

        # print(precl.values.shape)

        del lons, new_lats, new_lons  # , ttime


    ######## delaware ######## 3

    arq_nc = f'{script_dir}/../data/br/' \
        f'delaware_precip_mon_total_0.5x0.5_198101-201112_v501.nc'
    
    print(f'input:  {arq_nc}')
    
    with xr.open_dataset(arq_nc) as dset:

        # dado delaware em cm
        delaware = dset['precip'][:, ::-1, :] * 10

        delaware.values[np.isnan(delaware.values)] = -999.0

        # ttime = delaware.coords['time'].values

        lons = delaware.coords['lon'].values

        new_lats = delaware.coords['lat'].values

        new_delaware, new_lons = shiftgrid(lons[0], delaware.values, 
                                           lons, start=False)

        fout = f'{script_dir}/../data/br-accum-mensal/' \
            f'delaware_precip_mon_total_0.5x0.5_198101-201112_v501.nc'

        writenc_mon(new_delaware, new_lats, new_lons, fname=fout, 
                    ntimes=new_delaware.shape[0])

        print(f'output: {fout}')

        # delaware = xr.DataArray(new_delaware, 
        #                         coords=[ttime, new_lats, new_lons], 
        #                         dims=['time', 'lat', 'lon'])

        # print(delaware.coords['lon'].values)

        # print(delaware.coords['lat'].values)

        # print(delaware.values.shape)

        del lons, new_lats, new_lons  # , ttime


    ######## chirps ######## 4

    arq_nc = f'{script_dir}/../data/br/' \
        f'chirps_precip_mon_total_0.05x0.05_198101-201112_v2.0.nc'

    print(f'input:  {arq_nc}')

    with xr.open_dataset(arq_nc) as dset:

        chirps = dset['precip']

        chirps.values[np.isnan(chirps.values)] = -999.0

        # ttime = chirps.coords['time'].values

        new_lons = chirps.coords['lon'].values

        new_lats = chirps.coords['lat'].values

        fout = f'{script_dir}/../data/br-accum-mensal/' \
            f'chirps_precip_mon_total_0.05x0.05_198101-201112_v2.0.nc'

        writenc_mon(chirps.values, new_lats, new_lons, fname=fout, 
                    ntimes=chirps.values.shape[0])

        print(f'output: {fout}')

        # chirps = xr.DataArray(chirps.values, 
        #                       coords=[ttime, new_lats, new_lons], 
        #                       dims=['time', 'lat', 'lon'])

        # print(chirps.coords['lon'].values)

        # print(chirps.coords['lat'].values)

        # print(chirps.values.shape)

        # del new_lats, new_lons  # , ttime


    ######## gpcc ######## 5

    arq_nc = f'{script_dir}/../data/br/' \
        f'gpcc_precip_mon_total_0.5x0.5_198101-201112_v7.nc'

    print(f'input:  {arq_nc}')

    with xr.open_dataset(arq_nc) as dset:

        gpcc = dset['precip'][:, ::-1, :]

        gpcc.values[np.isnan(gpcc.values)] = -999.0

        # ttime = gpcc.coords['time'].values

        lons = gpcc.coords['lon'].values

        new_lats = gpcc.coords['lat'].values

        new_gpcc, new_lons = shiftgrid(lons[0], gpcc.values, 
                                       lons, start=False)

        fout = f'{script_dir}/../data/br-accum-mensal/' \
            f'gpcc_precip_mon_total_0.5x0.5_198101-201112_v7.nc'

        writenc_mon(new_gpcc, new_lats, new_lons, fname=fout, 
                    ntimes=new_gpcc.shape[0])

        print(f'output: {fout}')

        # gpcc = xr.DataArray(new_gpcc, 
        #                     coords=[ttime, new_lats, new_lons], 
        #                     dims=['time', 'lat', 'lon'])

        # print(gpcc.coords['lon'].values)

        # print(gpcc.coords['lat'].values)

        # print(gpcc.values.shape)

        del lons, new_lats, new_lons  # , ttime


    ######## cru ######## 6

    arq_nc = f'{script_dir}/../data/br/' \
        f'cru_pre_mon_total_0.5x0.5_198101-201112.nc'
    
    print(f'input:  {arq_nc}')
    
    with xr.open_dataset(arq_nc) as dset:

        cru = dset['pre']

        cru.values[np.isnan(cru.values)] = -999.0

        # ttime = cru.coords['time'].values

        new_lons = cru.coords['lon'].values

        new_lats = cru.coords['lat'].values

        fout = f'{script_dir}/../data/br-accum-mensal/' \
            f'cru_pre_mon_total_0.5x0.5_198101-201112.nc'

        writenc_mon(cru.values, new_lats, new_lons, fname=fout, 
                    ntimes=cru.values.shape[0])

        print(f'output: {fout}')

        # cru = xr.DataArray(cru.values, 
        #                    coords=[ttime, new_lats, new_lons], 
        #                    dims=['time', 'lat', 'lon'])

        # print(cru.coords['lon'].values)

        # print(cru.coords['lat'].values)

        # print(cru.values.shape)

        del new_lats, new_lons  # , ttime


    ######## xavier ######## 7

    arq_nc = f'{script_dir}/../data/br/' \
        f'xavier_prec_daily_total_0.25x0.25_19810101-20111231.nc'
    
    print(f'input:  {arq_nc}')
    
    with xr.open_dataset(arq_nc) as dset:

        # acumulado mensal
        xavier = dset['prec'].resample(time='1M').sum('time', skipna=False)

        xavier.values[np.isnan(xavier.values)] = -999.0

        # ttime = xavier.coords['time'].values

        new_lons = xavier.coords['lon'].values

        new_lats = xavier.coords['lat'].values

        fout = f'{script_dir}/../data/br-accum-mensal/' \
            f'xavier_prec_mon_total_0.25x0.25_198101-201112.nc'

        writenc_mon(xavier.values, new_lats, new_lons, fname=fout, 
                    ntimes=xavier.values.shape[0])

        print(f'output: {fout}')

        # xavier = xr.DataArray(xavier.values, 
        #                       coords=[ttime, new_lats, new_lons], 
        #                       dims=['time', 'lat', 'lon'])

        # print(xavier.coords['lon'].values)

        # print(xavier.coords['lat'].values)

        # print(xavier.values.shape)

        del new_lats, new_lons  # , ttime


    ######## sa24 ######## 8

    arq_nc = f'{script_dir}/../data/br/' \
        f'sa24_precip_daily_total_1x1_19810101-20111231.nc'
    
    print(f'input:  {arq_nc}')
    
    with xr.open_dataset(arq_nc) as dset:

        # acumulado mensal
        sa24 = dset['precip'].resample(time='1M').sum('time', skipna=False)

        sa24.values[np.isnan(sa24.values)] = -999.0

        # ttime = sa24.coords['time'].values

        new_lons = sa24.coords['lon'].values

        new_lats = sa24.coords['lat'].values

        fout = f'{script_dir}/../data/br-accum-mensal/' \
            f'sa24_precip_mon_total_1x1_198101-201112.nc'

        writenc_mon(sa24.values, new_lats, new_lons, fname=fout, 
                    ntimes=sa24.values.shape[0])

        print(f'output: {fout}')

        # sa24 = xr.DataArray(sa24.values, 
        #                     coords=[ttime, new_lats, new_lons], 
        #                     dims=['time', 'lat', 'lon'])

        # print(sa24.coords['lon'].values)

        # print(sa24.coords['lat'].values)

        # print(sa24.values.shape)

        del new_lats, new_lons  # , ttime


    ######## cpc ######## 9

    arq_nc = f'{script_dir}/../data/br/' \
        f'cpc_precip_daily_total_0.5x0.5_19810101-20111231.nc'

    print(f'input:  {arq_nc}')

    with xr.open_dataset(arq_nc) as dset:

        cpc = dset['precip'].drop([np.datetime64('2007-02-26')], dim='time')

        # acumulado mensal
        cpc = cpc[:, ::-1, :].resample(time='1M').sum('time', skipna=False)

        cpc.values[np.isnan(cpc.values)] = -999.0

        # ttime = cpc.coords['time'].values

        lons = cpc.coords['lon'].values

        new_lats = cpc.coords['lat'].values

        new_cpc, new_lons = shiftgrid(lons[0], cpc.values, 
                                      lons, start=False)

        fout = f'{script_dir}/../data/br-accum-mensal/' \
            f'cpc_precip_mon_total_0.5x0.5_198101-201112.nc'

        writenc_mon(new_cpc, new_lats, new_lons, fname=fout, 
                    ntimes=new_cpc.shape[0])

        print(f'output: {fout}')

        # cpc = xr.DataArray(new_cpc, 
        #                    coords=[ttime, new_lats, new_lons], 
        #                    dims=['time', 'lat', 'lon'])

        # print(cpc.coords['lon'].values)

        # print(cpc.coords['lat'].values)

        # print(cpc.values.shape)

        del lons, new_lats, new_lons  # , ttime


    ######## gpcp ######## 10

    arq_nc = f'{script_dir}/../data/br/' \
        f'gpcp_precip_mon_mean_2.5x2.5_198101-201112.nc'

    print(f'input:  {arq_nc}')

    with xr.open_dataset(arq_nc, decode_times=False) as dset:

        gpcp = dset['precip']

        # novo eixo do tempo
        ttime = pd.date_range('1981-01-01', '2011-12-01', freq='MS')

        lons = gpcp.coords['lon'].values

        new_lats = gpcp.coords['lat'].values

        new_gpcp, new_lons = shiftgrid(lons[0], gpcp.values, 
                                       lons, start=False)

        gpcp = xr.DataArray(new_gpcp, coords=[ttime, new_lats, new_lons], 
                            dims=['time', 'lat', 'lon'])

        # acumulado mensal
        gpcp = gpcp * gpcp.time.dt.daysinmonth

        gpcp.values[np.isnan(gpcp.values)] = -999.0

        fout = f'{script_dir}/../data/br-accum-mensal/' \
            f'gpcp_precip_mon_total_2.5x2.5_198101-201112.nc'

        writenc_mon(gpcp.values, new_lats, new_lons, fname=fout, 
                    ntimes=gpcp.values.shape[0])

        print(f'output: {fout}')

        # gpcp = xr.DataArray(gpcp, coords=[ttime, new_lats, new_lons], 
        #                     dims=['time', 'lat', 'lon'])

        # print(gpcp.coords['lon'].values)

        # print(gpcp.coords['lat'].values)

        # print(gpcp.values.shape)

        del lons, new_lats, new_lons, ttime

    # return {'CMAP 2.5x2.5': cmap, 'GPCP 2.5x2.5': gpcp, 'SA24 1.0x1.0': sa24,
    #         'PRECL 0.5x0.5': precl, 'DELAWARE 0.5x0.5': delaware,
    #         'GPCC 0.5x0.5': gpcc, 'CRU 0.5x0.5': cru, 'CPC 0.5x0.5': cpc,
    #         'XAVIER 0.25x0.25': xavier, 'CHIRPS 0.05x0.05': chirps}


if __name__ == "__main__":
    # m será um dicionaário
    m = load_obs_data()
    # ofile = '../data/obs_accum_mon_1981-2011.p'
    # with open(ofile, 'wb') as fp:
    #     pickle.dump(m, fp, protocol=pickle.HIGHEST_PROTOCOL)
    # print(f'Arq salvo: {ofile}')
