#!/usr/bin/env python3.6

import matplotlib as mpl
mpl.use('Agg')

import os
import xarray as xr
import cartopy as cart
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import BoundaryNorm

script_dir = os.path.dirname(os.path.realpath(__file__))

data_ini, data_fim = '1981-01', '2010-12'

def corrigir_coords(lat, lon):
    # correção no grade para pcolormesh
    deltalat = np.mean(np.diff(lat)) / 2.
    deltalon = np.mean(np.diff(lon)) / 2.
    newlat = lat - deltalat
    newlon = lon - deltalon
    return newlat, newlon

# def mycolorbar(mappable):
#     ax = mappable.axes
#     fig = ax.figure
#     divider = make_axes_locatable(ax)
#     cax = divider.append_axes("right", size="5%", pad=0.05)
#     return fig.colorbar(mappable, cax=cax)

print('* Lendo arquivos e calculando acumulados anuais...')

######## cmap ######## 1

arq_nc = f'{script_dir}/../data/original/br/cmap_precip_mon_mean_2.5x2.5_198101-201112.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_cmap = dset['precip']
    # faz acumulado mensal
    precip_cmap = precip_cmap * precip_cmap.time.dt.daysinmonth
    # faz acumulado anual
    precip_cmap = precip_cmap.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_cmap['lat'].values, precip_cmap['lon'].values)
precip_cmap_anual = precip_cmap.assign_coords(lon=lon, lat=lat)

######## precl ######## 2

arq_nc = f'{script_dir}/../data/original/br/precl_precip_mon_mean_0.5x0.5_198101-201112.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_precl = dset['precip']
    precip_precl = precip_precl * precip_precl.time.dt.daysinmonth
    precip_precl = precip_precl.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_precl['lat'].values, precip_precl['lon'].values)
precip_precl = precip_precl.assign_coords(lon=lon, lat=lat)

######## delaware ######## 3

arq_nc = f'{script_dir}/../data/original/br/delaware_precip_mon_total_0.5x0.5_198101-201112_v501.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_delaware = dset['precip'] * 10
    # precip_delaware = precip_delaware * precip_delaware.time.dt.daysinmonth
    precip_delaware = precip_delaware.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_delaware['lat'].values, precip_delaware['lon'].values)
precip_delaware = precip_delaware.assign_coords(lon=lon, lat=lat)

######## chirps ######## 4

arq_nc = f'{script_dir}/../data/original/br/chirps_precip_mon_total_0.5x0.5_198101-201112_v2.0.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_chirps = dset['precip']
    precip_chirps = precip_chirps.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_chirps['lat'].values, precip_chirps['lon'].values)
precip_chirps = precip_chirps.assign_coords(lon=lon, lat=lat)

######## gpcc ######## 5

arq_nc = f'{script_dir}/../data/original/br/gpcc_precip_mon_total_0.5x0.5_198101-201112_v7.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_gpcc = dset['precip']
    precip_gpcc = precip_gpcc.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_gpcc['lat'].values, precip_gpcc['lon'].values)
precip_gpcc = precip_gpcc.assign_coords(lon=lon, lat=lat)

######## cru ######## 6

arq_nc = f'{script_dir}/../data/original/br/cru_pre_mon_total_0.5x0.5_198101-201112.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_cru = dset['pre']
    precip_cru = precip_cru.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_cru['lat'].values, precip_cru['lon'].values)
precip_cru = precip_cru.assign_coords(lon=lon, lat=lat)

######## xavier ######## 7

arq_nc = f'{script_dir}/../data/original/br/xavier_prec_daily_total_0.25x0.25_19810101-20111231.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_xavier = dset['prec']
    precip_xavier = precip_xavier.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
    # precip_xavier_m = precip_xavier.sel(time=slice(data_ini, data_fim)).resample(time='1M').sum('time')
    # precip_xavier_y = precip_xavier_m.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_xavier['lat'].values, precip_xavier['lon'].values)
precip_xavier = precip_xavier.assign_coords(lon=lon, lat=lat)

######## sa24 ######## 8

arq_nc = f'{script_dir}/../data/original/br/sa24_precip_daily_total_1x1_19810101-20111231.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_sa24 = dset['precip']
    precip_sa24 = precip_sa24.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_sa24['lat'].values, precip_sa24['lon'].values)
precip_sa24 = precip_sa24.assign_coords(lon=lon, lat=lat)

######## cpc ######## 9

arq_nc = f'{script_dir}/../data/original/br/cpc_precip_daily_total_0.5x0.5_19810101-20111231.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_cpc = dset['precip']
    precip_cpc = precip_cpc.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_cpc['lat'].values, precip_cpc['lon'].values)
precip_cpc = precip_cpc.assign_coords(lon=lon, lat=lat)

ano = 1981

# ######## stats ########
# print('\n **** Check ****')
# print(' => cmap', 'min:', precip_cmap.sel(year=ano).min().values, 'max:', precip_cmap.sel(year=ano).max().values)
# print(' => precl', 'min:', precip_precl.min().values, 'max:', precip_precl.max().values)
# print(' => delaware', 'min:', precip_delaware.min().values, 'max:', precip_delaware.max().values)
# print(' => chirps', 'min:', precip_chirps.min().values, 'max:', precip_chirps.max().values)
# print(' => gpcc', 'min:', precip_gpcc.min().values, 'max:', precip_gpcc.max().values)
# print(' => cru', 'min:', precip_cru.min().values, 'max:', precip_cru.max().values)
# print(' => xavier', 'min:', precip_xavier.min().values, 'max:', precip_xavier.max().values)
# print(' => sa24', 'min:', precip_sa24.min().values, 'max:', precip_sa24.max().values)
# print(' => cpc', 'min:', precip_cpc.min().values, 'max:', precip_cpc.max().values)

# TODO: fazer verificação com min e max
# TODO: shapes com linha mais fina

states = cart.feature.NaturalEarthFeature(category='cultural',
                            scale='50m', facecolor='none',
                            name='admin_1_states_provinces_shp',
                            linewidth=0.5)

countries = cart.feature.NaturalEarthFeature(category='cultural',
                        scale='50m', facecolor='none',
                        name='admin_0_countries',
                        linewidth=0.5)

# x1, x2, y1, y2 = 318, 323, -2, -8.5  # CE
# x1, x2, y1, y2 = 310, 331, 5, -19    # NEB
x1, x2, y1, y2 = 273, 331, -61, 16     # AS

bases_obs = [precip_cmap, precip_sa24, precip_precl, precip_delaware,
             precip_chirps, precip_gpcc, precip_cru, precip_cpc,
             precip_xavier]

bases_nomes = ['CMAP 2.5x2.5', 'SA24 1.0x1.0', 'PRECL 0.5x0.5',
               'DELAWARE 0.5x0.5', 'CHIRPS 0.5x0.5', 'GPCC 0.5x0.5',
               'CRU 0.5x0.5', 'CPC 0.5x0.5', 'XAVIER 0.25x0.25']

fig, axes = plt.subplots(nrows=2, ncols=5,
                         subplot_kw={'projection': cart.crs.PlateCarree(central_longitude=300)},
                         figsize=(20, 10))

clevs = list(range(0, 3001, 200))
cmap = plt.get_cmap('jet_r')  # gray_r
norm = BoundaryNorm(clevs, ncolors=cmap.N, clip=True)

fig.suptitle(f'{ano}', fontsize=16)

for ax, baseobs, bases_nome in zip(axes.ravel(), bases_obs, bases_nomes):

    d1 = ax.pcolormesh(baseobs['lon'], baseobs['lat'],
                       baseobs.sel(year=ano),
                       transform=cart.crs.PlateCarree(),
                       cmap=cmap, norm=norm)

    ax.coastlines(resolution='50m', linewidth=0.5)
    # ax.set_xticks([80, 70, 60, 50, 40], crs=cart.crs.PlateCarree())
    ax.add_feature(states, edgecolor='k', linewidth=0.5)
    ax.add_feature(countries, edgecolor='k', linewidth=0.5)
    ax.add_feature(cart.feature.OCEAN, zorder=50, edgecolor='k', facecolor='w', linewidth=0.5)
    ax.set_extent([x1, x2, y1, y2], cart.crs.PlateCarree())
    ax.set_title(bases_nome, fontsize=10, fontweight='bold')
    ax.set_aspect('auto', adjustable=None)

    cbar = fig.colorbar(d1, ax=ax, ticks=clevs, pad=0.02, boundaries=clevs)
    cbar.ax.tick_params(labelsize=8)

    g1 = ax.gridlines(draw_labels=True, linewidth=0.01)
    g1.xlabels_top = False
    g1.ylabels_right = False
    g1.xlabels_bottom = True
    g1.ylabels_left = True
    g1.xformatter = cart.mpl.gridliner.LONGITUDE_FORMATTER
    g1.yformatter = cart.mpl.gridliner.LATITUDE_FORMATTER

# fig.subplots_adjust(hspace=0.8, wspace=0.4)

fig_outdir = f'{script_dir}/../figs/acum_anual/accum_anual_{ano}.png'
print(fig_outdir)
plt.savefig(fig_outdir, dpi=200, bbox_inches='tight')

# plt.show()

plt.close()

# raise SystemExit
