#!/usr/bin/env python3.6

import os
import xarray as xr
import numpy as np
from colorama import Fore, Back, Style

from salva_trimestres import salva_trimestres


script_dir = os.path.dirname(os.path.realpath(__file__))

def trimestres(ifile, ofile, data_ini = '1981-01-01', data_fim = '2011-03-01'):

    print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

    with xr.open_dataset(ifile) as dset:

        var = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                            .rolling(time=3) \
                            .sum()

        lat = dset['lat'].values

        lon = dset['lon'].values

        trimestres = var[2:].values

        trimestres = trimestres.reshape(30, 12, len(lat), len(lon))

        # a função substitui NaN por -999.0 que será o _Fillvalue
        salva_trimestres(trimestres, lat, lon, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

###### CMAP ######

ifile = f'{script_dir}/../data/br-accum-mensal' \
        f'/cmap_precip_mon_total_2.5x2.5_198101-201112.nc'

ofile = f'{script_dir}/../data/trimestres_acumulados' \
        f'/cmap_trimestres_acumulados_1981-2010.nc'

trimestres(ifile, ofile)


###### GPCP ######

ifile = f'{script_dir}/../data/br-accum-mensal' \
        f'/gpcp_precip_mon_total_2.5x2.5_198101-201112.nc'

ofile = f'{script_dir}/../data/trimestres_acumulados' \
        f'/gpcp_trimestres_acumulados_1981-2010.nc'

trimestres(ifile, ofile)


###### SA24 ######

ifile = f'{script_dir}/../data/br-accum-mensal' \
        f'/sa24_precip_mon_total_1x1_198101-201112.nc'

ofile = f'{script_dir}/../data/trimestres_acumulados' \
        f'/sa24_trimestres_acumulados_1981-2010.nc'

trimestres(ifile, ofile)


###### PRECL ######

ifile = f'{script_dir}/../data/br-accum-mensal' \
        f'/precl_precip_mon_total_0.5x0.5_198101-201112.nc'

ofile = f'{script_dir}/../data/trimestres_acumulados' \
        f'/precl_trimestres_acumulados_1981-2010.nc'

trimestres(ifile, ofile)


###### DELAWARE ######

ifile = f'{script_dir}/../data/br-accum-mensal' \
        f'/delaware_precip_mon_total_0.5x0.5_198101-201112_v501.nc'

ofile = f'{script_dir}/../data/trimestres_acumulados' \
        f'/delaware_trimestres_acumulados_1981-2010.nc'

trimestres(ifile, ofile)


###### GPCC ######

ifile = f'{script_dir}/../data/br-accum-mensal' \
        f'/gpcc_precip_mon_total_0.5x0.5_198101-201112_v7.nc'

ofile = f'{script_dir}/../data/trimestres_acumulados' \
        f'/gpcc_trimestres_acumulados_1981-2010.nc'

trimestres(ifile, ofile)


###### CRU ######

ifile = f'{script_dir}/../data/br-accum-mensal' \
        f'/cru_pre_mon_total_0.5x0.5_198101-201112.nc'

ofile = f'{script_dir}/../data/trimestres_acumulados' \
        f'/cru_trimestres_acumulados_1981-2010.nc'

trimestres(ifile, ofile)


###### CPC ######

ifile = f'{script_dir}/../data/br-accum-mensal' \
        f'/cpc_precip_mon_total_0.5x0.5_198101-201112.nc'

ofile = f'{script_dir}/../data/trimestres_acumulados' \
        f'/cpc_trimestres_acumulados_1981-2010.nc'

trimestres(ifile, ofile)


###### XAVIER ######

ifile = f'{script_dir}/../data/br-accum-mensal' \
        f'/xavier_prec_mon_total_0.25x0.25_198101-201112.nc'

ofile = f'{script_dir}/../data/trimestres_acumulados' \
        f'/xavier_trimestres_acumulados_1981-2010.nc'

trimestres(ifile, ofile)


###### CHIRPS ######

ifile = f'{script_dir}/../data/br-accum-mensal' \
        f'/chirps_precip_mon_total_0.05x0.05_198101-201112_v2.0.nc'

ofile = f'{script_dir}/../data/trimestres_acumulados' \
        f'/chirps_trimestres_acumulados_1981-2010.nc'

trimestres(ifile, ofile)
