#!/usr/bin/env python3.6

import matplotlib as mpl
mpl.use('Agg')

import os
import numpy as np
import xarray as xr
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.basemap import maskoceans
from matplotlib import colors as c
from matplotlib.colors import BoundaryNorm
from matplotlib.patches import PathPatch
from matplotlib.path import Path
import matplotlib.pyplot as plt

script_dir = os.path.dirname(os.path.realpath(__file__))

def fixcoords(lats, lons):
    # correção nas coordenadas para plotas usando o pcolormesh
    deltalat = np.mean(np.diff(lats)) / 2.
    deltalon = np.mean(np.diff(lons)) / 2.
    lats = lats - deltalat
    lons = lons - deltalon
    return lats, lons

dados = {'CMAP 2.5x2.5': 'cmap_media_anual_1981-2010.nc',
         'GPCP 2.5x2.5': 'gpcp_media_anual_1981-2010.nc',
         'SA24 1x1': 'sa24_media_anual_1981-2010.nc',
         'CPC 0.5x0.5': 'cpc_media_anual_1981-2010.nc',
         'CRU 0.5x0.5': 'cru_media_anual_1981-2010.nc',
         'DELAWARE 0.5x0.5': 'delaware_media_anual_1981-2010.nc',
         'GPCC 0.5x0.5': 'gpcc_media_anual_1981-2010.nc',
         'PRECL 0.5x0.5': 'precl_media_anual_1981-2010.nc',
         'XAVIER 0.25x0.25': 'xavier_media_anual_1981-2010.nc',
         'CHIRPS 0.05x0.05': 'chirps_media_anual_1981-2010.nc'}

##### INICIO PLOTS #####

for ano in range(1981, 2011):

    fig = plt.figure(figsize=(30, 10))

    fig_pos = 1
    fig_row = 2
    fig_col = 5

    fig.suptitle(f'ACUMULADO ANUAL - {ano}', fontsize='18', fontweight='bold')

    for titulo, nome_nc in dados.items():

        print(nome_nc)

        ifile = f'{script_dir}/../data/media_anual/{nome_nc}'

        with xr.open_dataset(ifile) as dset:
                obs = dset['pr'].sel(time=f'{ano}-01-15')

        plt.subplot(fig_row, fig_col, fig_pos)

        lats = obs.coords['lat'].values

        lons = obs.coords['lon'].values

        lats, lons = fixcoords(lats, lons)

        mymap = Basemap(projection='cyl', llcrnrlat=-35.0, urcrnrlat=7.0,
                        llcrnrlon=-75.0, urcrnrlon=-33, suppress_ticks=True)

        mymap.drawmeridians(np.arange(-160., 161., 10.), labels=[0, 0, 0, 1],
                            linewidth=0.001, fontsize=10, fontweight='bold')

        mymap.drawparallels(np.arange(-90., 91., 10.), labels=[1, 0, 0, 0],
                            linewidth=0.001, fontsize=10, fontweight='bold')

        lons, lats = np.meshgrid(lons, lats)

        x, y = mymap(lons, lats)

        # jet_r
        cbar = ('#800000', '#D10000', '#FF3000', '#FF7300', '#FFB600',
                '#F1FC06', '#B7FF40', '#7DFF7A', '#43FFB4', '#09F1EE',
                '#00A4FF', '#005DFF', '#0015FF', '#0000D6', '#000080')

        my_cmap = c.ListedColormap(cbar)

        my_cmap.set_over('#551A8B')

        my_cmap.set_bad('#696969')

        clevs = list(range(0, 301, 20))

        # cmap = plt.get_cmap('jet_r')  # gray_r
        norm = BoundaryNorm(clevs, ncolors=my_cmap.N, clip=False)

        cs = plt.pcolormesh(x, y, obs[0, :, :], cmap=my_cmap, norm=norm)

        xyshp = np.loadtxt('./shp/br.asc', delimiter=',')

        path = Path(xyshp)

        patch = PathPatch(path, facecolor='none', lw=0.0)

        plt.gca().add_patch(patch)

        cs.set_clip_path(patch)

        mymap.readshapefile('./shp/brazil', './shp/brazil',
                            drawbounds=True, linewidth=.7, color='k')

        plt.title(titulo, fontsize=14, weight='bold')

        fig_pos += 1

    fig.subplots_adjust(right=0.84)

    xax = -0.145   # pos x
    yax = 0.11     # pos y
    xss = 1
    yss = 0.77
    bl = 'right'
    xs = '1.2%'  # esperssura da paleta

    cbar_ax = fig.add_axes([xax, yax, xss, yss])

    cbar_ax.spines['left'].set_visible(False)
    cbar_ax.spines['right'].set_visible(False)
    cbar_ax.spines['top'].set_visible(False)
    cbar_ax.spines['bottom'].set_visible(False)
    cbar_ax.xaxis.set_visible(False)
    cbar_ax.yaxis.set_visible(False)
    cbar_ax.set_facecolor('none')

    cbar = mymap.colorbar(cs, location=bl, spacing='uniform',
                          extendfrac='auto', ticks=clevs, extend='max',
                          pad='0%', size=xs)

    # https://goo.gl/38q45G
    cbar.ax.tick_params(labelsize=10, direction='in')
    
    # http://bit.ly/2TLHMSE
    # cbar.ax.set_ylabel('TESTE', fontsize=22, weight='bold')
    cbar.ax.set_yticklabels(clevs, fontsize=14, weight='bold')

    nome_fig = f'{script_dir}/../figs/media_anual/media_anual_{ano}.png'

    print(f'Salvando: {nome_fig}')

    plt.savefig(nome_fig, dpi=300)  # , bbox_inches='tight')

    plt.close()

