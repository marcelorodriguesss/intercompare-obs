from netCDF4 import Dataset, num2date
from netcdftime import datetime as datetimex, date2num
from datetime import datetime, timedelta

def writenc_mon(data, lat, lon, fname='out.nc', timeunits='days', 
                timecalendar='360_day', ntimes=372, 
                year=1981, month=1, day=15):

    foo = Dataset(fname, 'w', format='NETCDF4_CLASSIC')

    foo.createDimension('time', None)
    foo.createDimension('lat', len(lat))
    foo.createDimension('lon', len(lon))

    lats = foo.createVariable('lat', 'f4', ('lat'), zlib=True)
    lats.units = 'degrees_north'
    lats.long_name = 'lat'
    lats.axis = 'Y'
    lats[:] = lat[:]

    lons = foo.createVariable('lon', 'f4', ('lon'), zlib=True)
    lons.units = 'degrees_east'
    lons.long_name = 'lon'
    lons.axis = 'X'
    lons[:] = lon[:]

    times = foo.createVariable('time', 'f4', ('time'), zlib=True)
    times.units = f'{timeunits} since {year}-{month:02d}-{day:02d} 00:00:00'
    times.calendar = timecalendar
    times.standard_name = 'time'
    times.long_name = 'time'
    times.axis = 'T'

    dates = []
    for n in list(range(ntimes)):
        new_date = date2num(datetime(year, month, day), units=times.units, calendar=times.calendar) + n * 30
        dates.append(num2date(new_date, units=times.units, calendar=times.calendar))
    times[:] = date2num(dates, units=times.units, calendar=times.calendar)

    var = foo.createVariable('pr', float, ('time', 'lat', 'lon'), fill_value=-999.0, zlib=True)
    var.units = 'mm'
    var.long_name = 'Total Precipitation'
    var.missing_value = -999.0
    var[:] = data[:]

    foo.comment = 'Try to recreate data'

    foo.close()
