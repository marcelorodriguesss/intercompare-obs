#!/usr/bin/env python3.6

from save_nc import EscreveNC
import xarray as xr

arq_entrada = '/home/rodrigues/gitlab/intercompare-obs' \
              '/data/original/gpcp_precip_mon_mean_2.5x2.5_197901-201901.nc'

with xr.open_dataset(arq_entrada) as dset:
    # print(dset)
    pr = dset['precip']
    print(pr)

print(arq_entrada)

arq_saida = '/home/rodrigues/gitlab/intercompare-obs' \
            '/scripts/gpcp_precip_mon_mean_2.5x2.5_197901-201901.nc'

novo_nc = EscreveNC()
novo_nc.fname = arq_saida
novo_nc.lat = pr['lat'].values
novo_nc.lon = pr['lon'].values
novo_nc.tempo = list(range(481))
novo_nc.dados3d = pr.values
novo_nc.varnomecurto = 'precip'
novo_nc.varunidade = 'mm/day'
novo_nc.varnomelongo = 'Average Monthly Rate of Precipitation'
novo_nc.tempounidade = 'months since 1979-01-15 00:00:00'
novo_nc.comentario = 'Try to recreate'

# print(novo_nc.__dict__)

novo_nc.escrevenc()

print(arq_saida)

