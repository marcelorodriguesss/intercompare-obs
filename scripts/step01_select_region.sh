#!/usr/bin/env bash

# seleciona a região de interesse
# seleciona os anos de interesse

CDO_CMD='cdo sellonlatbox,-90,-25,-65,20 -selyear,1981/2011'

ARQS_NC=(
    "cmap_precip_mon_mean_2.5x2.5_197901-201901.nc"
    "cru_pre_mon_total_0.5x0.5_198101-201712.nc"
    "delaware_precip_mon_total_0.5x0.5_190001-201712_v501.nc"
    "gpcc_precip_mon_total_0.5x0.5_190101-201312_v7.nc"
    "precl_precip_mon_mean_0.5x0.5_194801-201201.nc"
    "sa24_precip_daily_total_1x1_19400101-20120430.nc"
    "xavier_prec_daily_total_0.25x0.25_19800101-20151231.nc"
    "chirps_precip_mon_total_0.5x0.5_198101-201901_v2.0.nc"
    "cpc_precip_daily_total_0.5x0.5_19790101-20190224.nc"
    "gpcp_precip_mon_mean_2.5x2.5_197901-201901.nc"
)

WDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd ${WDIR}/../data/original

mkdir br 2>/dev/null

for I in "${ARQS_NC[@]}"; do
    echo "${CDO_CMD} ${I} br/${I}"
    ${CDO_CMD} ${I} br/${I}
done

echo " +++ FIM DA TRANSMISSÃO +++"
echo

