import numpy as np
from netCDF4 import Dataset, num2date
from netcdftime import datetime as datetimex, date2num
from datetime import datetime, timedelta

def salva_trimestres(data, lat, lon, fname='out.nc'):

    data[np.isnan(data)] = -999.0

    foo = Dataset(fname, 'w', format='NETCDF4_CLASSIC')

    foo.createDimension('time', 30)
    foo.createDimension('lead', 12)
    foo.createDimension('lat', len(lat))
    foo.createDimension('lon', len(lon))

    lats = foo.createVariable('lat', 'f4', ('lat'), zlib=True)
    lats.units = 'degrees_north'
    lats.long_name = 'lat'
    lats.axis = 'Y'
    lats[:] = lat[:]

    lons = foo.createVariable('lon', 'f4', ('lon'), zlib=True)
    lons.units = 'degrees_east'
    lons.long_name = 'lon'
    lons.axis = 'X'
    lons[:] = lon[:]

    times = foo.createVariable('time', 'i', ('time'), zlib=True)
    times.long_name = 'time'
    times.axis = 'T'
    times[:] = list(range(1981, 2011))

    season = foo.createVariable('lead', 'i', ('lead'), zlib=True)
    season.long_name = 'lead'
    season.axis = 'K'
    season[:] = list(range(12))

    var = foo.createVariable('pr', float, ('time', 'lead', 'lat', 'lon'),
                             fill_value=-999.0, zlib=True)
    var.units = 'mm'
    var.long_name = 'Precipitation Accumulated'
    var.missing_value = -999.0
    var[:] = data[:]

    foo.comment = 'Precip Acumulada por trimestres.'

    foo.close()
