from mpl_toolkits.basemap import Basemap
from mpl_toolkits.basemap import maskoceans
from matplotlib import colors as c
from matplotlib.colors import BoundaryNorm
from matplotlib.patches import PathPatch
from matplotlib.path import Path
from netCDF4 import Dataset
import os
from PIL import Image
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt


def maptercis(below, normal, above, lat, lon, **kwargs):

    maptype = kwargs.get('maptype', 'shade')
    fig_title = kwargs.get('fig_title', 'My Title')
    y1 = kwargs.get('y1', -60.)
    y2 = kwargs.get('y2', 15.)
    x1 = kwargs.get('x1', -90.)
    x2 = kwargs.get('x2', -30.)
    resol = kwargs.get('resol', 'c')
    paral = kwargs.get('parallels', np.arange(-90., 91., 10.))
    merid = kwargs.get('meridians', np.arange(-160., 161., 10.))
    fig_name = kwargs.get('fig_name', 'probs_fig.png')
    dpi = kwargs.get('dpi', 200)
    dstk = kwargs.get('dstk', 'as')
    xticks_names = kwargs.get('xticks_names', ('Abaixo', 'Normal', 'Acima'))
    dimcrops = kwargs.get('dimcrops', (1050, 785, 1630, 1550))
    logopos = kwargs.get('logopos', (437, 583))

    belowp = np.where(((below > normal) & (below > above)), below, 0.)
    normalp = np.where(((normal > above) & (normal > below)), normal + 100., 0.)
    abovep = np.where(((above > normal) & (above > below)), above + 1000., 0.)
    finalp = (belowp + normalp + abovep)

    # ajuste nos pontos do plot
    if maptype == 'shade':
        deltalat = np.mean(np.diff(lat)) / 2.
        deltalon = np.mean(np.diff(lon)) / 2.
        lat = lat - deltalat
        lon = lon - deltalon

    # ref: http://www.python-course.eu/matplotlib_multiple_figures.php

    ####### BELOW #######

    fig = plt.figure(figsize=(14, 12))

    plt.subplot(2, 2, 1)

    mymap = Basemap(projection='cyl', llcrnrlat=y1, urcrnrlat=y2, llcrnrlon=x1,
                    urcrnrlon=x2, resolution=resol, suppress_ticks=True)

    mymap.drawmeridians(merid, labels=[0, 0, 0, 1], linewidth=0.001,
                        fontsize=6)

    mymap.drawparallels(paral, labels=[1, 0, 0, 0], linewidth=0.001,
                        fontsize=6)

    lons, lats = np.meshgrid(lon, lat)

    x, y = mymap(lons, lats)

    barcol = ('#ffed4c', '#f9bb34', '#f75026', '#dd2b28')

    cpalover = barcol[-1]
    barcol = barcol[:-1]
    my_cmap = c.ListedColormap(barcol)
    my_cmap.set_over(cpalover)
    my_cmap.set_under('#ffffff')

    levs = (40., 50., 60, 70.)

    norm = BoundaryNorm(levs, ncolors=my_cmap.N, clip=False)

    cs1 = plt.pcolormesh(x, y, belowp, cmap=my_cmap, norm=norm)  # vmin=40.

    local_dir = os.path.dirname(__file__)

    mymap.readshapefile(local_dir + '/shp/world', '/shp/world',
                        drawbounds=True, linewidth=.5, color='k')

    plt.title('{0}'.format(xticks_names[0].upper()), fontsize=9)

    ####### NORMAL #######

    plt.subplot(2, 2, 2)

    mymap = Basemap(projection='cyl', llcrnrlat=y1, urcrnrlat=y2, llcrnrlon=x1,
                    urcrnrlon=x2, resolution=resol, suppress_ticks=True)

    mymap.drawmeridians(merid, labels=[0, 0, 0, 1], linewidth=0.001,
                        fontsize=6)

    mymap.drawparallels(paral, labels=[1, 0, 0, 0], linewidth=0.001,
                        fontsize=6)

    lons, lats = np.meshgrid(lon, lat)

    x, y = mymap(lons, lats)

    barcol = ('#ccf2cc', '#abf2aa', '#57f255', '#16f200')

    cpalover = barcol[-1]
    barcol = barcol[:-1]
    my_cmap = c.ListedColormap(barcol)
    my_cmap.set_over(cpalover)
    my_cmap.set_under('#ffffff')

    levs = (40., 50., 60, 70.)

    norm = BoundaryNorm(levs, ncolors=my_cmap.N, clip=False)

    cs2 = plt.pcolormesh(x, y, normalp - 100, cmap=my_cmap, norm=norm)  # vmin=40.

    local_dir = os.path.dirname(__file__)

    mymap.readshapefile(local_dir + '/shp/world', '/shp/world',
                        drawbounds=True, linewidth=.5, color='k')

    plt.title('{0}'.format(xticks_names[1].upper()), fontsize=9)

    ####### ABOVE #######

    plt.subplot(2, 2, 3)

    mymap = Basemap(projection='cyl', llcrnrlat=y1, urcrnrlat=y2, llcrnrlon=x1,
                    urcrnrlon=x2, resolution=resol, suppress_ticks=True)

    mymap.drawmeridians(merid, labels=[0, 0, 0, 1], linewidth=0.001,
                        fontsize=6)

    mymap.drawparallels(paral, labels=[1, 0, 0, 0], linewidth=0.001,
                        fontsize=6)

    lons, lats = np.meshgrid(lon, lat)

    x, y = mymap(lons, lats)

    barcol = ('#b2b2f2', '#8484f2', '#4143f2', '#0006f2')

    cpalover = barcol[-1]
    barcol = barcol[:-1]
    my_cmap = c.ListedColormap(barcol)
    my_cmap.set_over(cpalover)
    my_cmap.set_under('#ffffff')

    levs = (40., 50., 60, 70.)

    norm = BoundaryNorm(levs, ncolors=my_cmap.N, clip=False)

    cs3 = plt.pcolormesh(x, y, abovep - 1000, cmap=my_cmap, norm=norm)

    local_dir = os.path.dirname(__file__)

    mymap.readshapefile(local_dir + '/shp/world', '/shp/world',
                        drawbounds=True, linewidth=.5, color='k')

    plt.title('{0}'.format(xticks_names[2].upper()), fontsize=9)

    ####### TERCIS #######

    plt.subplot(2, 2, 4)

    mymap = Basemap(projection='cyl', llcrnrlat=y1, urcrnrlat=y2, llcrnrlon=x1,
                    urcrnrlon=x2, resolution=resol, suppress_ticks=True)

    mymap.drawmeridians(merid, labels=[0, 0, 0, 1], linewidth=0.001,
                        fontsize=6)

    mymap.drawparallels(paral, labels=[1, 0, 0, 0], linewidth=0.001,
                        fontsize=6)

    lons, lats = np.meshgrid(lon, lat)

    x, y = mymap(lons, lats)

    barcol = (
        '#ffffff', '#ffed4c', '#f9bb34', '#f75026', '#dd2b28', '#dd2b28',
        '#ffffff', '#ccf2cc', '#abf2aa', '#57f255', '#16f200', '#16f200',
        '#ffffff', '#b2b2f2', '#8484f2', '#4143f2', '#0006f2', '#0006f2'
    )

    my_cmap = c.ListedColormap(barcol)

    levs = (0., 40., 50., 60, 70., 100.,
            100.1, 140., 150., 160., 170., 1000.,
            1000.1, 1040., 1050., 1060., 1070., 1100)

    norm = BoundaryNorm(levs, ncolors=my_cmap.N, clip=False)

    cs = plt.pcolormesh(x, y, finalp, cmap=my_cmap, norm=norm)

    # ref: https://goo.gl/fJw74X

    local_dir = os.path.dirname(__file__)

    if dstk == 'as':

        pathtxt = '{0}/shp/as.asc'.format(local_dir)
        xyshp = np.loadtxt(pathtxt, delimiter=',')
        path = Path(xyshp)
        patch = PathPatch(path, facecolor='none', lw=0.0)
        plt.gca().add_patch(patch)
        cs.set_clip_path(patch)

    mymap.readshapefile(local_dir + '/shp/world', '/shp/world',
                        drawbounds=True, linewidth=.5, color='k')

    plt.title(fig_title, fontsize=9)

    fig.subplots_adjust(bottom=0.1, right=0.84)

    # ajustes das barras de cores

    xax = .578  # pos x
    yax = .06  # pos y
    xss = .06
    yss = .07
    bl = 'bottom'
    xs = '18%'  # esperssura das paletas

    ###

    cbar_ax = fig.add_axes([xax, yax, xss, yss])

    cbar_ax.spines['left'].set_visible(False)
    cbar_ax.spines['right'].set_visible(False)
    cbar_ax.spines['top'].set_visible(False)
    cbar_ax.spines['bottom'].set_visible(False)
    cbar_ax.xaxis.set_visible(False)
    cbar_ax.yaxis.set_visible(False)
    # cbar_ax.set_axis_bgcolor('none')
    cbar_ax.set_facecolor('none')

    cbar = mymap.colorbar(cs1, location=bl, spacing='uniform',
                          extendfrac='auto', ticks=levs, extend='max',
                          pad='10%', size=xs)

    # https://goo.gl/38q45G
    cbar.ax.tick_params(labelsize=6, direction='in')

    ###

    cbar_ax = fig.add_axes([xax + .07, yax, xss, yss])

    cbar_ax.spines['left'].set_visible(False)
    cbar_ax.spines['right'].set_visible(False)
    cbar_ax.spines['top'].set_visible(False)
    cbar_ax.spines['bottom'].set_visible(False)
    cbar_ax.xaxis.set_visible(False)
    cbar_ax.yaxis.set_visible(False)
    # cbar_ax.set_axis_bgcolor('none')
    cbar_ax.set_facecolor('none')

    cbar = mymap.colorbar(cs2, location=bl, spacing='uniform',
                          extendfrac='auto', ticks=levs, extend='max',
                          pad='10%', size=xs)

    cbar.ax.tick_params(labelsize=6, direction='in')

    ###

    cbar_ax = fig.add_axes([xax + .14, yax, xss, yss])

    cbar_ax.spines['left'].set_visible(False)
    cbar_ax.spines['right'].set_visible(False)
    cbar_ax.spines['top'].set_visible(False)
    cbar_ax.spines['bottom'].set_visible(False)
    cbar_ax.xaxis.set_visible(False)
    cbar_ax.yaxis.set_visible(False)
    # cbar_ax.set_axis_bgcolor('none')
    cbar_ax.set_facecolor('none')

    cbar = mymap.colorbar(cs3, location=bl, spacing='uniform',
                          extendfrac='auto', ticks=levs, extend='max',
                          pad='10%', size=xs)

    cbar.ax.tick_params(labelsize=6, direction='in')

    ###
    plt.text(-2.1, 0, '{0}'.format(xticks_names[0].upper()), fontsize=7)
    plt.text(-1.0, 0, '{0}'.format(xticks_names[1].upper()), fontsize=7)
    plt.text(0.2, 0, '{0}'.format(xticks_names[2].upper()), fontsize=7)

    plt.show()

    plt.savefig(fig_name, dpi=dpi)

    plt.close()
