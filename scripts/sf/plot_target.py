#!/usr/bin/env python3.6

import matplotlib.pyplot as plt
from matplotlib import rcParams
import numpy as np
from pprint import pprint
import pickle
import skill_metrics as sm
from sys import version_info

def load_obj(pickle_name_file):
    with open(pickle_name_file, 'rb') as fin:
        return pickle.load(fin)


class Container(object):
    def __init__(self, target_stats1, target_stats2, taylor_stats1, taylor_stats2):
        self.target_stats1 = target_stats1
        self.target_stats2 = target_stats2
        self.taylor_stats1 = taylor_stats1
        self.taylor_stats2 = taylor_stats2

if __name__ == '__main__':

    # Set the figure properties (optional)
    rcParams["figure.figsize"] = [8.0, 6.4]
    rcParams['lines.linewidth'] = 1 # line width for plots
    rcParams.update({'font.size': 12}) # font size of axes text

    # Close any previously open graphics windows
    # ToDo: fails to work within Eclipse
    plt.close('all')

    # Read target statistics for ERA Interim (stats1) and TRMM (stats2)
    # data with respect to APHRODITE observations for each of years 2001 to
    # 2014 from pickle file
    # stats = load_obj('thiessen_vies_baixo.pkl') # observations
    stats = load_obj('thiessen_vies_baixo_nopercent.pkl') # observations

    pprint(stats)
    print(np.sort(stats['cmap']))
    # print(stats.target_stats1['crmsd'])
    # print(stats.target_stats1['rmsd'])

    # raise SystemExit

    # Specify labels for points in a dictionary because only desire labels
    # for each data set.
    # label = {'ERA-5': 'r', 'TRMM': 'b'}
    label = {'CMAP': 'r'}

    '''
    Produce the target diagram for the first dataset
    '''
    sm.target_diagram(stats['cmap'],
                      stats['cmap'],
                      stats['cmap'], markercolor ='r', alpha = 0.0,
                      ticks = np.arange(-5, 35, 5),
                      circles = np.arange(-5, 35, 5),
                      circleLineSpec = 'k--', circleLineWidth = 1.0)

    '''
    Overlay the second dataset
    '''
    # sm.target_diagram(stats.target_stats2['bias'],
    #                   stats.target_stats2['crmsd'],
    #                   stats.target_stats2['rmsd'], markercolor ='b', alpha = 0.0,
    #                   overlay = 'on', markerLabel = label)

    # Write plot to file
    plt.savefig('target8.png', dpi=100, facecolor='w')

    # Show plot
    plt.show()
