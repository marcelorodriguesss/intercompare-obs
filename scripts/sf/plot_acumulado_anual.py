#!/usr/bin/env python3.6

import os
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from scipy.interpolate import spline

from pfct import Thiessen as thi

script_dir = os.path.dirname(os.path.realpath(__file__))

# acumulado anual

bases = ['cmap', 'gpcp', 'precl', 'gpcc', 'cpc',
         'delaware', 'cru', 'xavier', 'chirps']

bacias = ['alto', 'medio', 'baixo']

for bac in bacias:

    print(f'{bac}')

    bacia = f'{script_dir}/../shp/sf/{bac}/sao_francisco_{bac}.txt'

    my_list = []

    for b in bases:

        print(f'  {b}')

        d = f'{script_dir}/../../data/acumulado_anual/{b}_acumulado_anual_1981-2010.nc'

        with xr.open_dataset(d) as dset:
            # print(dset)
            # dset['pr'].sel(time='1981', lon=slice(-54, -29), lat=slice(-26, -2)).plot()
            pr = dset['pr'].sel(lon=slice(-54, -29), lat=slice(-26, -2))
            lats = pr.coords['lat'].values
            lons = pr.coords['lon'].values

            if b == 'cmap' or b == 'gpcp':
                pf = 1
            else:
                pf = -1

            res = thi.thiessen(pr.values, lats, lons, bacia, pf=pf, sep=' ',
                               usenc=True, figname=f'figs/{b}_{bac}.png')

            my_list.append(res[0])

    anos = np.arange(1981, 2011)

    fig, ax = plt.subplots(figsize=(12, 5))

    x_smooth = np.linspace(anos.min(), anos.max(), 300)

    ###

    y_smooth = spline(anos, my_list[0], x_smooth)

    ax.plot(x_smooth, y_smooth, color='green', linestyle='dashed',
            linewidth=2, label='cmap')

    ###

    y_smooth = spline(anos, my_list[1], x_smooth)

    ax.plot(x_smooth, y_smooth, color='red', linestyle='dashed',
            linewidth=2, label='gpcp')

    ###

    y_smooth = spline(anos, my_list[2], x_smooth)

    ax.plot(x_smooth, y_smooth, color='orange', linestyle='dashed',
            linewidth=2, label='precl')

    ###

    y_smooth = spline(anos, my_list[3], x_smooth)

    ax.plot(x_smooth, y_smooth, color='blue', linestyle='dashed',
            linewidth=2, label='gpcc')

    ###

    y_smooth = spline(anos, my_list[4], x_smooth)

    ax.plot(x_smooth, y_smooth, color='purple', linestyle='dashed',
            linewidth=2, label='cpc')

    ###

    y_smooth = spline(anos, my_list[5], x_smooth)

    ax.plot(x_smooth, y_smooth, color='yellow', linestyle='dashed',
            linewidth=2, label='delaware')

    ###

    y_smooth = spline(anos, my_list[6], x_smooth)

    ax.plot(x_smooth, y_smooth, color='turquoise', linestyle='dashed',
            linewidth=2, label='cru')

    ###

    y_smooth = spline(anos, my_list[7], x_smooth)

    ax.plot(x_smooth, y_smooth, color='violet', linestyle='dashed',
            linewidth=2, label='xavier')

    ###

    y_smooth = spline(anos, my_list[8], x_smooth)

    ax.plot(x_smooth, y_smooth, color='teal', linestyle='dashed',
            linewidth=2, label='chirps')

    ###

    ax.legend(loc='upper right', ncol=5)  # , bbox_to_anchor=(1.15, 1.0))

    plt.xticks(anos, rotation=90)

    # plt.yticks(np.arange(0, 1801, 200))

    # plt.grid()

    plt.xlabel('\nANOS')

    plt.ylabel('PRECIPTAÇÃO (mm)\n')

    plt.title(f'ACUMULADO ANUAL - {bac.upper()} JAGUARIBE')

    plt.tight_layout()

    # plt.show()

    plt.savefig(f'resultados/acumulado_anual_{bac}.png', dpi=200)

    plt.close()
