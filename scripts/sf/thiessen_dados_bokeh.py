#!/usr/bin/env python3.6

import os
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from scipy.interpolate import spline

from pfct import Thiessen as thi

script_dir = os.path.dirname(os.path.realpath(__file__))

# acumulado anual

bases = ['cmap'] # , 'gpcp', 'precl', 'gpcc', 'cpc',
         # 'delaware', 'cru', 'xavier']  # , 'chirps']

bacias = ['baixo'] # , 'medio', 'alto']

for bac in bacias:

    bacia = f'{script_dir}/../shp/sf/{bac}/sao_francisco_{bac}.txt'

    my_list = []

    for b in bases:

        d = f'{script_dir}/../../data/acumulado_anual/{b}_acumulado_anual_1981-2010.nc'

        with xr.open_dataset(d) as dset:
            print(dset)
            var = dset['pr'].values
            lats = dset['pr'].coords['lat'].values
            lons = dset['pr'].coords['lon'].values

            res = thi.thiessen(var, lats, lons, bacia, pf=-1, sep=' ',
                               usenc=True, figname=f'{b}_{bac}.png')

            my_list.append(res[0])

    print(my_list)

    anos = np.arange(1981, 2011)

    # anos = np.array([1, 2, 3, 4])
    # my_list = [np.array([1,2,8,12])]

    print(anos)
    print(my_list[0])

    # raise SystemExit

    x_smooth = np.linspace(anos.min(), anos.max(), 300)

    y_smooth = spline(anos, my_list[0], x_smooth)

    plt.plot(x_smooth, y_smooth)

    plt.show()
