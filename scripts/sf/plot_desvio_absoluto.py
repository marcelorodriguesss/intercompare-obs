#!/usr/bin/env python3.6

import matplotlib.pyplot as plt
from matplotlib import rcParams
import numpy as np
from pprint import pprint
import pickle
import skill_metrics as sm
from sys import version_info

def load_obj(pickle_name_file):
    with open(pickle_name_file, 'rb') as fin:
        return pickle.load(fin)


stats = load_obj('thiessen_vies_baixo_nopercent.pkl') # observations

pprint(stats)
# print(np.sort(stats['cmap']))
print(stats['cmap'])
print(stats['funceme0p25'])

x1 = stats['cmap'][0]
x2 = stats['gpcp'][0]
x3 = stats['cpc'][0]

y = stats['funceme0p25'][0]

y = 1

print(x1)
print(x2)
print(x3)
print(y)

fig, ax = plt.subplots(figsize=(12, 5))

ax.scatter(x1, y, label='CMAP')
ax.scatter(x2, y, label='GPCP')
ax.scatter(x3, y, label='CPC')

# ax.annotate('17.09', (x[0], x[0]))

# ax.plot(x1, y, linestyle='dashed')

ax.legend(loc='upper right')  # , ncol=5)  # , bbox_to_anchor=(1.15, 1.0))

# http://bit.ly/2UkHlip

# ax.set_xticks(meses)

# ax.set_xticklabels(['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'])

# plt.xticks(np.arange(0, 40))

# plt.yticks(np.arange(0, 1801, 200))

# plt.grid()

# plt.xlabel('\nMESES')

# plt.ylabel('PRECIPTAÇÃO (mm)\n')

# plt.title(f'CLIMATOLOGIA MENSAL - {bac.upper()} JAGUARIBE')

plt.tight_layout()

plt.show()

# plt.savefig(f'resultados/climatologia_mensal_{bac}.png', dpi=200)

plt.close()


