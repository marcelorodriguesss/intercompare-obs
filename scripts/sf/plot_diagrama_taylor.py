#!/usr/bin/env python3.6

import os
import pickle
import numpy as np
import xarray as xr
import pandas as pd
from pprint import pprint
import matplotlib.pyplot as plt
from scipy.interpolate import spline
from scipy.stats.stats import pearsonr

from pfct import Thiessen as thi

from taylor_diagram import TaylorDiagram

script_dir = os.path.dirname(os.path.realpath(__file__))

nomes_bases = ['funceme0p25', 'cmap', 'gpcp', 'precl', 'gpcc',
               'cpc', 'delaware', 'cru', 'xavier']  # 'chirps'

arq_ncs = [
    'funceme_pr_mon_total_0.25x0.25_198101-201112.nc',
    'cmap_precip_mon_total_2.5x2.5_198101-201112.nc',
    'gpcp_precip_mon_total_2.5x2.5_198101-201112.nc',
    'precl_precip_mon_total_0.5x0.5_198101-201112.nc',
    'gpcc_precip_mon_total_0.5x0.5_198101-201112_v7.nc',
    'cpc_precip_mon_total_0.5x0.5_198101-201112.nc',
    'delaware_precip_mon_total_0.5x0.5_198101-201112_v501.nc',
    'cru_pre_mon_total_0.5x0.5_198101-201112.nc',
    'xavier_prec_mon_total_0.25x0.25_198101-201112.nc'
]

# 'chirps_precip_mon_total_0.05x0.05_198101-201112_v2.0.nc'

bacias = ['baixo']  # , 'medio', 'alto']

for bacia in bacias:

    contorno_bacia = f'{script_dir}/../shp/sf/{bacia}' \
                     f'/sao_francisco_{bacia}.txt'

    # dicionario com o thiessen calculado para todas as bases
    thiessen_bases = {}

    for i, nc in enumerate(arq_ncs):

        ncfile = f'{script_dir}/../../data/acumulado_mensal/{nc}'

        with xr.open_dataset(ncfile) as dset:

            pr   = dset['pr'].sel(lon=slice(-54, -29), lat=slice(-26, -2),
                                  time=slice('1981-01', '2010-12'))
            var  = pr.values
            lats = pr.coords['lat'].values
            lons = pr.coords['lon'].values

            print(f' + {ncfile}')

            if nomes_bases[i] == 'cmap' or nomes_bases[i] == 'gpcp':
                pf = 1
            else:
                pf = -1

            res = thi.thiessen(var, lats, lons, contorno_bacia,
                               pf=pf, sep=' ', usenc=True,
                               figname=f'figs/{nomes_bases[i]}_{bacia}.png')

            thiessen_bases[nomes_bases[i]] = res[0, :]

    dates_rng = pd.date_range('1981/01/01', '2010/12/01', freq='MS')

    df = pd.DataFrame(thiessen_bases, index=dates_rng)

    ### CALCULA CORRELAÇÃO DAS BASES COM FUNCENE

    corr_jan = (df[df.index.month == 1]).corr(method='pearson')
    corr_fev = (df[df.index.month == 2]).corr(method='pearson')
    corr_mar = (df[df.index.month == 3]).corr(method='pearson')
    corr_abr = (df[df.index.month == 4]).corr(method='pearson')
    corr_mai = (df[df.index.month == 5]).corr(method='pearson')
    corr_jun = (df[df.index.month == 6]).corr(method='pearson')
    corr_jul = (df[df.index.month == 7]).corr(method='pearson')
    corr_ago = (df[df.index.month == 8]).corr(method='pearson')
    corr_set = (df[df.index.month == 9]).corr(method='pearson')
    corr_out = (df[df.index.month == 10]).corr(method='pearson')
    corr_nov = (df[df.index.month == 11]).corr(method='pearson')
    corr_dez = (df[df.index.month == 12]).corr(method='pearson')

    # colocar correl e std de cada base por gráfico

    ### NORMALIZANDO AS BASES PELA MÉDIA E DESVIO DA FUNCEME

    media_ref = []
    desvio_ref = []

    for mes in range(1, 13):
        res_media = (df['funceme0p25'][df.index.month == mes]).mean()
        res_std = (df['funceme0p25'][df.index.month == mes]).std()
        media_ref.append(res_media)
        desvio_ref.append(res_std)

    jan_norm = (df[df.index.month == 1] - media_ref[0]) / desvio_ref[0]
    fev_norm = (df[df.index.month == 2] - media_ref[1]) / desvio_ref[1]
    mar_norm = (df[df.index.month == 3] - media_ref[2]) / desvio_ref[2]
    abr_norm = (df[df.index.month == 4] - media_ref[3]) / desvio_ref[3]
    mai_norm = (df[df.index.month == 5] - media_ref[4]) / desvio_ref[4]
    jun_norm = (df[df.index.month == 6] - media_ref[5]) / desvio_ref[5]
    jul_norm = (df[df.index.month == 7] - media_ref[6]) / desvio_ref[6]
    ago_norm = (df[df.index.month == 8] - media_ref[7]) / desvio_ref[7]
    set_norm = (df[df.index.month == 9] - media_ref[8]) / desvio_ref[8]
    out_norm = (df[df.index.month == 10] - media_ref[9]) / desvio_ref[9]
    nov_norm = (df[df.index.month == 11] - media_ref[10]) / desvio_ref[10]
    dez_norm = (df[df.index.month == 12] - media_ref[11]) / desvio_ref[11]

    ### DESVIO PADRÃO DA SÉRIE NORMALIZADA
    ### SERÁ USADO NO DIAGRAM DE TAYLOR

    std_jan_norm = jan_norm.std()
    std_fev_norm = fev_norm.std()
    std_mar_norm = mar_norm.std()
    std_abr_norm = abr_norm.std()
    std_mai_norm = mai_norm.std()
    std_jun_norm = jun_norm.std()
    std_jul_norm = jul_norm.std()
    std_ago_norm = ago_norm.std()
    std_set_norm = set_norm.std()
    std_out_norm = out_norm.std()
    std_nov_norm = nov_norm.std()
    std_dez_norm = dez_norm.std()

    # test
    # std_jan_norm =  df['cmap'][df.index.month == 1].std() / df['funceme0p25'][df.index.month == 1].std()

    ### DIAGRAMA DE TAYLOR

    # Reference std
    stdref = 1.0

    # Samples std, corr, name
    samples = [
        [std_jan_norm['cmap'], corr_jan['cmap'][corr_jan.index == 'funceme0p25'], 'CMAP'],
        [std_jan_norm['gpcp'], corr_jan['gpcp'][corr_jan.index == 'funceme0p25'], 'GPCP'],
        [std_jan_norm['precl'], corr_jan['precl'][corr_jan.index == 'funceme0p25'], 'PRECL'],
        [std_jan_norm['gpcc'], corr_jan['gpcc'][corr_jan.index == 'funceme0p25'], 'GPCC'],
        [std_jan_norm['cpc'], corr_jan['cpc'][corr_jan.index == 'funceme0p25'], 'CPC'],
        [std_jan_norm['delaware'], corr_jan['delaware'][corr_jan.index == 'funceme0p25'], 'DELAWARE'],
        [std_jan_norm['cru'], corr_jan['cru'][corr_jan.index == 'funceme0p25'], 'CRU'],
        [std_jan_norm['xavier'], corr_jan['xavier'][corr_jan.index == 'funceme0p25'], 'XAVIER'],
    ]

    # [std_jan_norm['chirps'], corr_jan['chirps'][corr_jan.index == 'funceme0p25'], 'CHIRPS']

    fig = plt.figure(figsize=(8, 8))

    dia = TaylorDiagram(stdref, fig=fig, label='REFERÊNCIA', extend=False)

    dia.samplePoints[0].set_color('r')  # Mark reference point as a red star

    # Add models to Taylor diagram
    for i, (stddev, corrcoef, name) in enumerate(samples):
        dia.add_sample(stddev, corrcoef,
                       marker='$%d$' % (i+1), ms=10, ls='',
                       mfc='k', mec='k',
                       label=name)

    # Add RMS contours, and label them
    contours = dia.add_contours(levels=3, colors='0.5')  # 3 levels in grey
    plt.clabel(contours, inline=1, fontsize=10, fmt='%.0f')

    dia.add_grid()                                  # Add grid
    dia._ax.axis[:].major_ticks.set_tick_out(True)  # Put ticks outward

    # Add a figure legend and title
    fig.legend(dia.samplePoints,
               [ p.get_label() for p in dia.samplePoints ],
               numpoints=1, prop=dict(size='small'), loc='upper right')

    fig.suptitle(f'{bacia.upper()} SÃO FRANCISCO - JANEIRO\n\n', size='x-large')

    plt.tight_layout()

    plt.savefig('baixosf_janeiro.png', dpi=200)

    # plt.show()

    plt.close()
