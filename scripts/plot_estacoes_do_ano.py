#!/usr/bin/env python3.6

import matplotlib as mpl
mpl.use('Agg')

import os
import numpy as np
import xarray as xr
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.basemap import maskoceans
from matplotlib import colors as c
from matplotlib.colors import BoundaryNorm
from matplotlib.patches import PathPatch
from matplotlib.path import Path
import matplotlib.pyplot as plt

# um ou outro
compute_mean = 1
compute_std = 0

script_dir = os.path.dirname(os.path.realpath(__file__))

def fixcoords(lats, lons):
    # correção nas coordenadas para plotas usando o pcolormesh
    deltalat = np.mean(np.diff(lats)) / 2.
    deltalon = np.mean(np.diff(lons)) / 2.
    lats = lats - deltalat
    lons = lons - deltalon
    return lats, lons

dados = {'CMAP 2.5x2.5': 'cmap_trimestres_acumulados_1981-2010.nc',
         'GPCP 2.5x2.5': 'gpcp_trimestres_acumulados_1981-2010.nc',
         'GPCC 0.5x0.5': 'gpcc_trimestres_acumulados_1981-2010.nc',
         'PRECL 0.5x0.5': 'precl_trimestres_acumulados_1981-2010.nc',
         'CPC 0.5x0.5': 'cpc_trimestres_acumulados_1981-2010.nc',
         'CRU 0.5x0.5': 'cru_trimestres_acumulados_1981-2010.nc',
         'DELAWARE 0.5x0.5': 'delaware_trimestres_acumulados_1981-2010.nc',
         'SA24 1.0x1.0': 'sa24_trimestres_acumulados_1981-2010.nc',
         'XAVIER 0.25x0.25': 'xavier_trimestres_acumulados_1981-2010.nc',
         'CHIRPS 0.05x0.05': 'chirps_trimestres_acumulados_1981-2010.nc'}

##### INICIO PLOTS #####

# 11 DJF, 2 MAM, 5 JJA, 8 SON , 0 JFM, 1 FMA

dict_names = {11: 'DJF (VERÃO)', 2: 'MAM (OUTONO)', 5: 'JJA (INVERNO)',
               8: 'SON (PRIMAVERA)', 0: 'JFM', 1: 'FMA'}

fig_names = {11: 'DJF', 2: 'MAM', 5: 'JJA', 8: 'SON', 0: 'JFM', 1: 'FMA'}

for estacao in [11, 2, 5, 8, 0, 1, 11]:

    fig = plt.figure(figsize=(30, 10))

    fig_pos = 1
    fig_row = 2
    fig_col = 5

    fig.suptitle(f'CLIMATOLOGIA MÉDIA {dict_names[estacao]}', fontsize='18', fontweight='bold')

    for titulo, nome_nc in dados.items():

        print(nome_nc)

        ifile = f'{script_dir}/../data/trimestres_acumulados/{nome_nc}'

        with xr.open_dataset(ifile) as dset:

            if compute_mean:
                # seleciona a estação e faz a média sobre os anos (climatologia)
                obs = dset['pr'].sel(lead=estacao).mean('time')
            elif compute_std:
                # seleciona a estação e calcula o desvio padrao sobre os anos (climatologia)
                obs = dset['pr'].sel(lead=estacao).std('time')
            else:
                raise SystemExit('Verifique: compute_mean e compute_std')

        plt.subplot(fig_row, fig_col, fig_pos)

        lats = obs.coords['lat'].values

        lons = obs.coords['lon'].values

        lats, lons = fixcoords(lats, lons)

        mymap = Basemap(projection='cyl', llcrnrlat=-35.0, urcrnrlat=7.0,
                        llcrnrlon=-75.0, urcrnrlon=-33, suppress_ticks=True)

        mymap.drawmeridians(np.arange(-160., 161., 10.), labels=[0, 0, 0, 1],
                            linewidth=0.001, fontsize=10, fontweight='bold')

        mymap.drawparallels(np.arange(-90., 91., 10.), labels=[1, 0, 0, 0],
                            linewidth=0.001, fontsize=10, fontweight='bold')

        lons, lats = np.meshgrid(lons, lats)

        x, y = mymap(lons, lats)

        cbar = ('#a80000', '#d60000', '#ff1600', '#ff3b00', '#ff6000',
                '#ff8600', '#ffa700', '#ffcc00', '#fbf100', '#dbff1c',
                '#baff3c', '#9aff5d', '#7aff7d', '#5dff9a', '#3cffba',
                '#1cffdb', '#00e0fb', '#00b8ff', '#0090ff', '#006dff',
                '#0045ff', '#001dff', '#0000ff', '#0000d6', '#0000a8')

        my_cmap = c.ListedColormap(cbar)

        my_cmap.set_over('#3D1A7E')

        my_cmap.set_bad('#696969')

        if compute_mean:
            clevs = list(range(0, 1251, 50))
        elif compute_std:
            clevs = list(range(0, 251, 10))
        else:
            raise SystemExit('Verifique: compute_mean e compute_std')

        # cmap = plt.get_cmap('jet_r')  # gray_r
        norm = BoundaryNorm(clevs, ncolors=my_cmap.N, clip=False)

        cs = plt.pcolormesh(x, y, obs, cmap=my_cmap, norm=norm)

        xyshp = np.loadtxt('./shp/br.asc', delimiter=',')

        path = Path(xyshp)

        patch = PathPatch(path, facecolor='none', lw=0.0)

        plt.gca().add_patch(patch)

        cs.set_clip_path(patch)

        mymap.readshapefile('./shp/brazil', './shp/brazil',
                            drawbounds=True, linewidth=.7, color='k')

        plt.title(titulo, fontsize=14, weight='bold')

        fig_pos += 1

    fig.subplots_adjust(right=0.84)

    xax = -0.145   # pos x
    yax = 0.11     # pos y
    xss = 1
    yss = 0.77
    bl = 'right'
    xs = '1.2%'  # esperssura da paleta

    cbar_ax = fig.add_axes([xax, yax, xss, yss])

    cbar_ax.spines['left'].set_visible(False)
    cbar_ax.spines['right'].set_visible(False)
    cbar_ax.spines['top'].set_visible(False)
    cbar_ax.spines['bottom'].set_visible(False)
    cbar_ax.xaxis.set_visible(False)
    cbar_ax.yaxis.set_visible(False)
    cbar_ax.set_facecolor('none')

    cbar = mymap.colorbar(cs, location=bl, spacing='uniform',
                          extendfrac='auto', ticks=clevs, extend='max',
                          pad='0%', size=xs)

    # https://goo.gl/38q45G
    cbar.ax.tick_params(labelsize=10, direction='in')

    # http://bit.ly/2TLHMSE
    # cbar.ax.set_ylabel('TESTE', fontsize=22, weight='bold')
    cbar.ax.set_yticklabels(clevs, fontsize=14, weight='bold')

    if compute_mean:
        nome_fig = f'{script_dir}/../figs/estacoes_ano_media' \
                   f'/estacoes_ano_media_{fig_names[estacao]}.png'
    elif compute_std:
        nome_fig = f'{script_dir}/../figs/estacoes_ano_desvio_padrao' \
                   f'/estacoes_ano_desvio_padrao_{fig_names[estacao]}.png'
    else:
        raise SystemExit('Verifique: compute_mean e compute_std')

    print(f'Salvando: {nome_fig}')

    plt.savefig(nome_fig, dpi=300)  # , bbox_inches='tight')

    plt.close()
