#!/usr/bin/env python3.6

import os
import xarray as xr
import cartopy as cart
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import BoundaryNorm

script_dir = os.path.dirname(os.path.realpath(__file__))

data_ini, data_fim = '1981-01-01', '2010-12-01'

def corrigir_coords(lat, lon):
    # correção no grade para pcolormesh
    deltalat = np.mean(np.diff(lat)) / 2.
    deltalon = np.mean(np.diff(lon)) / 2.
    newlat = lat - deltalat
    newlon = lon - deltalon
    return newlat, newlon

print('Lendo arquivos e calculando acumulados anuais...')

######## cmap ######## 1

arq_nc = f'{script_dir}/../data/original/br/cmap_precip_mon_mean_2.5x2.5_197901-201901.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_cmap = dset['precip']
    # faz acumulado mensal
    precip_cmap = precip_cmap * precip_cmap.time.dt.daysinmonth
    # faz acumulado anual
    precip_cmap = precip_cmap.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_cmap['lat'].values, precip_cmap['lon'].values)
precip_cmap = precip_cmap.assign_coords(lon=lon, lat=lat)

######## precl ######## 2

arq_nc = f'{script_dir}/../data/original/br/precl_precip_mon_mean_0.5x0.5_194801-201201.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_precl = dset['precip']
    precip_precl = precip_precl * precip_precl.time.dt.daysinmonth
    precip_precl = precip_precl.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_precl['lat'].values, precip_precl['lon'].values)
precip_precl = precip_precl.assign_coords(lon=lon, lat=lat)

######## delaware ######## 3

arq_nc = f'{script_dir}/../data/original/br/delaware_precip_mon_total_0.5x0.5_190001-201712_v501.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_delaware = dset['precip']
    # precip_delaware = precip_delaware * precip_delaware.time.dt.daysinmonth
    precip_delaware = precip_delaware.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_delaware['lat'].values, precip_delaware['lon'].values)
precip_delaware = precip_delaware.assign_coords(lon=lon, lat=lat)

######## chirps ######## 4

arq_nc = f'{script_dir}/../data/original/br/chirps_precip_mon_total_0.5x0.5_198101-201901_v2.0.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_chirps = dset['precip']
    precip_chirps = precip_chirps.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_chirps['lat'].values, precip_chirps['lon'].values)
precip_chirps = precip_chirps.assign_coords(lon=lon, lat=lat)

######## gpcc ######## 5

arq_nc = f'{script_dir}/../data/original/br/gpcc_precip_mon_total_0.5x0.5_190101-201312_v7.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_gpcc = dset['precip']
    precip_gpcc = precip_gpcc.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_gpcc['lat'].values, precip_gpcc['lon'].values)
precip_gpcc = precip_gpcc.assign_coords(lon=lon, lat=lat)

######## cru ######## 6

arq_nc = f'{script_dir}/../data/original/br/cru_pre_mon_total_0.5x0.5_198101-201712.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_cru = dset['pre']
    precip_cru = precip_cru.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_cru['lat'].values, precip_cru['lon'].values)
precip_cru = precip_cru.assign_coords(lon=lon, lat=lat)

######## xavier ######## 7

arq_nc = f'{script_dir}/../data/original/br/xavier_prec_daily_total_0.25x0.25_19800101-20151231.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_xavier = dset['prec']
    precip_xavier = precip_xavier.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
    # precip_xavier_m = precip_xavier.sel(time=slice(data_ini, data_fim)).resample(time='1M').sum('time')
    # precip_xavier_y = precip_xavier_m.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_xavier['lat'].values, precip_xavier['lon'].values)
precip_xavier = precip_xavier.assign_coords(lon=lon, lat=lat)

######## sa24 ######## 8

arq_nc = f'{script_dir}/../data/original/br/sa24_precip_daily_total_1x1_19400101-20120430.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_sa24 = dset['precip']
    precip_sa24 = precip_sa24.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_sa24['lat'].values, precip_sa24['lon'].values)
precip_sa24 = precip_sa24.assign_coords(lon=lon, lat=lat)

######## cpc ######## 9

arq_nc = f'{script_dir}/../data/original/br/cpc_precip_daily_total_0.5x0.5_19790101-20190224.nc'
print(arq_nc)
with xr.open_dataset(arq_nc) as dset:
    precip_cpc = dset['precip']
    precip_cpc = precip_cpc.sel(time=slice(data_ini, data_fim)).groupby('time.year').sum('time')
lat, lon = corrigir_coords(precip_cpc['lat'].values, precip_cpc['lon'].values)
precip_cpc = precip_cpc.assign_coords(lon=lon, lat=lat)

######## stats ########
print('\n **** Check ****')
print(' => cmap', 'min:', precip_cmap.min().values, 'max:', precip_cmap.max().values)
print(' => precl', 'min:', precip_precl.min().values, 'max:', precip_precl.max().values)
print(' => delaware', 'min:', precip_delaware.min().values, 'max:', precip_delaware.max().values)
print(' => chirps', 'min:', precip_chirps.min().values, 'max:', precip_chirps.max().values)
print(' => gpcc', 'min:', precip_gpcc.min().values, 'max:', precip_gpcc.max().values)
print(' => cru', 'min:', precip_cru.min().values, 'max:', precip_cru.max().values)
print(' => xavier', 'min:', precip_xavier.min().values, 'max:', precip_xavier.max().values)
print(' => sa24', 'min:', precip_sa24.min().values, 'max:', precip_sa24.max().values)
print(' => cpc', 'min:', precip_cpc.min().values, 'max:', precip_cpc.max().values)

# raise SystemExit

# TODO: fazer verificação com min e max
# TODO: colocar figs 2x5 por ano
# TODO: mascarar oceano
# TODO: shapes com linha mais fina

states = cart.feature.NaturalEarthFeature(category='cultural',
                            scale='50m', facecolor='none',
                            name='admin_1_states_provinces_shp')

countries = cart.feature.NaturalEarthFeature(category='cultural',
                        scale='50m', facecolor='none',
                        name='admin_0_countries')

# x1, x2, y1, y2 = 318, 323, -2, -8.5  # CE
x1, x2, y1, y2 = 310, 331, 5, -19    # NEB

ano = 1981

# extent=[-42, 0, -32, 0.5]
fig, (ax1, ax2, ax3, ax4, ax5), (ax6) = plt.subplots(nrows=2, ncols=5, subplot_kw={'projection': cart.crs.PlateCarree(central_longitude=300)}, figsize=(10, 5))

clevs = range(0, 3001, 200)
cmap = plt.get_cmap('gray_r')
norm = BoundaryNorm(clevs, ncolors=cmap.N, clip=False)

d1 = ax1.pcolormesh(precip_cmap['lon'], precip_cmap['lat'],
                    precip_cmap.sel(year=ano),
                    transform=cart.crs.PlateCarree(),
                    cmap=cmap, norm=norm)
ax1.coastlines(resolution='50m')
# ax1.set_xticks([80, 70, 60, 50, 40], crs=cart.crs.PlateCarree())
ax1.add_feature(states, edgecolor='k')
ax1.add_feature(countries, edgecolor='k')
ax1.set_extent([x1, x2, y1, y2], cart.crs.PlateCarree())
ax1.add_feature(cart.feature.OCEAN, zorder=50, edgecolor='k', facecolor='w')
ax1.set_title(f'CMAP {ano} (2.5x2.5)')
# fig.colorbar(d1, ax=ax1, ticks=clevs, pad=0.02)
g1 = ax1.gridlines(draw_labels=True, linewidth=0.01)
g1.xlabels_top = False
g1.ylabels_right = False
g1.xlabels_bottom = False
g1.ylabels_left = False
g1.xformatter = cart.mpl.gridliner.LONGITUDE_FORMATTER
g1.yformatter = cart.mpl.gridliner.LATITUDE_FORMATTER

d2 = ax2.pcolormesh(precip_sa24['lon'], precip_sa24['lat'],
                    precip_sa24.sel(year=ano),
                    transform=cart.crs.PlateCarree(),
                    cmap=cmap, norm=norm)
ax2.coastlines(resolution='50m')
ax2.add_feature(states, edgecolor='k')
ax2.add_feature(countries, edgecolor='k')
ax2.set_extent([x1, x2, y1, y2], cart.crs.PlateCarree())
ax2.add_feature(cart.feature.OCEAN, zorder=50, edgecolor='k', facecolor='w')
ax2.set_title(f'SA24 {ano} (1.0x1.0)')
# fig.colorbar(d2, ax=ax2, ticks=clevs, pad=0.02)
g2 = ax2.gridlines(draw_labels=True, linewidth=0.01)
g2.xlabels_top = False
g2.ylabels_right = False
g2.xlabels_bottom = False
g2.ylabels_left = False
g2.xformatter = cart.mpl.gridliner.LONGITUDE_FORMATTER
g2.yformatter = cart.mpl.gridliner.LATITUDE_FORMATTER

d3 = ax3.pcolormesh(precip_precl['lon'], precip_precl['lat'],
                    precip_precl.sel(year=ano),
                    transform=cart.crs.PlateCarree(),
                    cmap=cmap, norm=norm)
ax3.coastlines(resolution='50m')
ax3.add_feature(states, edgecolor='k')
ax3.add_feature(countries, edgecolor='k')
ax3.set_extent([x1, x2, y1, y2], cart.crs.PlateCarree())
ax3.add_feature(cart.feature.OCEAN, zorder=50, edgecolor='k', facecolor='w')
ax3.set_title(f'PRECL {ano} (0.5x0.5)')
# fig.colorbar(d3, ax=ax3, ticks=clevs, pad=0.02)
g3 = ax3.gridlines(draw_labels=True, linewidth=0.01)
g3.xlabels_top = False
g3.ylabels_right = False
g3.xlabels_bottom = False
g3.ylabels_left = False
g3.xformatter = cart.mpl.gridliner.LONGITUDE_FORMATTER
g3.yformatter = cart.mpl.gridliner.LATITUDE_FORMATTER

d4 = ax4.pcolormesh(precip_delaware['lon'], precip_delaware['lat'],
                    precip_delaware.sel(year=ano),
                    transform=cart.crs.PlateCarree(),
                    cmap=cmap, norm=norm)
ax4.coastlines(resolution='50m')
ax4.add_feature(states, edgecolor='k')
ax4.add_feature(countries, edgecolor='k')
ax4.set_extent([x1, x2, y1, y2], cart.crs.PlateCarree())
ax4.add_feature(cart.feature.OCEAN, zorder=50, edgecolor='k', facecolor='w')
ax4.set_title(f'DELAWARE {ano} (0.5x0.5)')
# fig.colorbar(d4, ax=ax4, ticks=clevs, pad=0.02)
g4 = ax4.gridlines(draw_labels=True, linewidth=0.01)
g4.xlabels_top = False
g4.ylabels_right = False
g4.xlabels_bottom = False
g4.ylabels_left = False
g4.xformatter = cart.mpl.gridliner.LONGITUDE_FORMATTER
g4.yformatter = cart.mpl.gridliner.LATITUDE_FORMATTER

d5 = ax5.pcolormesh(precip_cru['lon'], precip_cru['lat'],
                    precip_cru.sel(year=ano),
                    transform=cart.crs.PlateCarree(),
                    cmap=cmap, norm=norm)
ax5.coastlines(resolution='50m')
ax5.add_feature(states, edgecolor='k')
ax5.add_feature(countries, edgecolor='k')
ax5.set_extent([x1, x2, y1, y2], cart.crs.PlateCarree())
ax5.add_feature(cart.feature.OCEAN, zorder=50, edgecolor='k', facecolor='w')
ax5.set_title(f'CRU {ano} (0.5x0.5)')
# fig.colorbar(d5, ax=ax5, ticks=clevs, pad=0.02)
g5 = ax5.gridlines(draw_labels=True, linewidth=0.01)
g5.xlabels_top = False
g5.ylabels_right = False
g5.xlabels_bottom = False
g5.ylabels_left = False
g5.xformatter = cart.mpl.gridliner.LONGITUDE_FORMATTER
g5.yformatter = cart.mpl.gridliner.LATITUDE_FORMATTER

fig.subplots_adjust(hspace=0.8, wspace=0.4)

plt.show()

plt.close()

# plt.savefig(f'{script_dir}/subplot.png', dpi=150, bbox_inches='tight')
