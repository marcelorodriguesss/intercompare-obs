from netCDF4 import Dataset

class EscreveNC:

    fname = 'out.nc'
    lat = False
    lon = False 
    tempo = False
    dados3d = False
    varnomecurto = 'precip'
    varunidade = 'mm'
    varnomelongo = 'total precip'
    tempounidade = 'hours since 1900-01-01 00:00:00'
    comentario = ''

    def escrevenc(self):
        foo = Dataset(self.fname, 'w', format='NETCDF4_CLASSIC')
        foo.createDimension('time', None)
        foo.createDimension('lat', len(self.lat))
        foo.createDimension('lon', len(self.lon))
        
        lats = foo.createVariable('lat', 'f4', ('lat'), zlib=True)
        lats.units = 'degrees_north'
        lats.long_name = 'lat'
        lats.axis = 'Y'
        lats[:] = self.lat[:]
        
        lons = foo.createVariable('lon', 'f4', ('lon'), zlib=True)
        lons.units = 'degrees_east'
        lons.long_name = 'lon'
        lons.axis = 'X'
        lons[:] = self.lon[:]
        
        times = foo.createVariable('time', 'f4', ('time'), zlib=True)
        times.units = self.tempounidade
        times.calendar = 'standard'
        times.standard_name = 'time'
        times[:] = self.tempo
        
        var = foo.createVariable(self.varnomecurto, float, ('time', 'lat', 'lon'), zlib=True)
        var.units = self.varunidade
        var.long_name = self.varnomelongo
        var[:] = self.dados3d[:]

        foo.comment = self.comentario 

        foo.close()

