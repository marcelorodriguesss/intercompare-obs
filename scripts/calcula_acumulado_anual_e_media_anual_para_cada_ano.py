#!/usr/bin/env python3.6

import os
import xarray as xr
from colorama import Fore, Back, Style

from writenc_yr import writenc_yr

# 1 = True, 0 = False
compute_accum = 1
compute_mean = 1

script_dir = os.path.dirname(os.path.realpath(__file__))

data_ini, data_fim = '1981-01-01', '2011-01-01'

###### CMAP ######

ifile = f'{script_dir}/../data/br-accum-mensal/' \
        f'cmap_precip_mon_total_2.5x2.5_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

with xr.open_dataset(ifile) as dset:

    if compute_accum:

        print(dset['pr'].sel(time=slice(data_ini, data_fim)) \
              .coords['time'].values)
        
        cmap = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                         .groupby('time.year') \
                         .sum('time', skipna=False)

        ofile = f'{script_dir}/../data/acumulado_anual/' \
                f'cmap_acumulado_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(cmap.values, cmap.coords['lat'].values, 
                   cmap.coords['lon'].values, fname=ofile,
                   ntimes=cmap.values.shape[0])

        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

    if compute_mean:

        cmap = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                         .groupby('time.year') \
                         .mean('time', skipna=False)

        ofile = f'{script_dir}/../data/media_anual/' \
                f'cmap_media_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(cmap.values, cmap.coords['lat'].values, 
                   cmap.coords['lon'].values, fname=ofile,
                   ntimes=cmap.values.shape[0])

        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


###### GPCP ######

ifile = f'{script_dir}/../data/br-accum-mensal/' \
        f'gpcp_precip_mon_total_2.5x2.5_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

with xr.open_dataset(ifile) as dset:

    if compute_accum:

        gpcp = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                         .groupby('time.year') \
                         .sum('time', skipna=False)

        ofile = f'{script_dir}/../data/acumulado_anual/' \
                f'gpcp_acumulado_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(gpcp.values, gpcp.coords['lat'].values, 
                   gpcp.coords['lon'].values, fname=ofile,
                   ntimes=gpcp.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

    if compute_mean:

        gpcp = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                         .groupby('time.year') \
                         .mean('time', skipna=False)

        ofile = f'{script_dir}/../data/media_anual/' \
                f'gpcp_media_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(gpcp.values, gpcp.coords['lat'].values, 
                   gpcp.coords['lon'].values, fname=ofile,
                   ntimes=gpcp.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


###### SA24 ######

ifile = f'{script_dir}/../data/br-accum-mensal/' \
        f'sa24_precip_mon_total_1x1_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

with xr.open_dataset(ifile) as dset:

    if compute_accum:

        sa24 = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                         .groupby('time.year') \
                         .sum('time', skipna=False)

        ofile = f'{script_dir}/../data/acumulado_anual/' \
                f'sa24_acumulado_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(sa24.values, sa24.coords['lat'].values, 
                   sa24.coords['lon'].values, fname=ofile,
                   ntimes=sa24.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

    if compute_mean:

        sa24 = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                         .groupby('time.year') \
                         .mean('time', skipna=False)

        ofile = f'{script_dir}/../data/media_anual/' \
                f'sa24_media_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(sa24.values, sa24.coords['lat'].values, 
                   sa24.coords['lon'].values, fname=ofile,
                   ntimes=sa24.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


###### PRECL ######

ifile = f'{script_dir}/../data/br-accum-mensal/' \
    f'precl_precip_mon_total_0.5x0.5_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

with xr.open_dataset(ifile) as dset:

    if compute_accum:

        precl = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                          .groupby('time.year') \
                          .sum('time', skipna=False)

        ofile = f'{script_dir}/../data/acumulado_anual/' \
                f'precl_acumulado_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(precl.values, precl.coords['lat'].values, 
                   precl.coords['lon'].values, fname=ofile,
                   ntimes=precl.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

    if compute_mean:

        precl = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                          .groupby('time.year') \
                          .mean('time', skipna=False)

        ofile = f'{script_dir}/../data/media_anual/' \
                f'precl_media_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(precl.values, precl.coords['lat'].values, 
                   precl.coords['lon'].values, fname=ofile,
                   ntimes=precl.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


###### DELAWARE ######

ifile = f'{script_dir}/../data/br-accum-mensal/' \
        f'delaware_precip_mon_total_0.5x0.5_198101-201112_v501.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

with xr.open_dataset(ifile) as dset:

    if compute_accum:

        delaware = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                             .groupby('time.year') \
                             .sum('time', skipna=False)

        ofile = f'{script_dir}/../data/acumulado_anual/' \
                f'delaware_acumulado_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(delaware.values, delaware.coords['lat'].values, 
                   delaware.coords['lon'].values, fname=ofile,
                   ntimes=delaware.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

    if compute_mean:

        delaware = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                             .groupby('time.year') \
                             .mean('time', skipna=False)

        ofile = f'{script_dir}/../data/media_anual/' \
                f'delaware_media_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(delaware.values, delaware.coords['lat'].values, 
                   delaware.coords['lon'].values, fname=ofile,
                   ntimes=delaware.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


###### GPCC ######

ifile = f'{script_dir}/../data/br-accum-mensal/' \
        f'gpcc_precip_mon_total_0.5x0.5_198101-201112_v7.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

with xr.open_dataset(ifile) as dset:

    if compute_accum:

        gpcc = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                         .groupby('time.year') \
                         .sum('time', skipna=False)

        ofile = f'{script_dir}/../data/acumulado_anual/' \
                f'gpcc_acumulado_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(gpcc.values, gpcc.coords['lat'].values, 
                   gpcc.coords['lon'].values, fname=ofile,
                   ntimes=gpcc.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

    if compute_mean:

        gpcc = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                         .groupby('time.year') \
                         .mean('time', skipna=False)

        ofile = f'{script_dir}/../data/media_anual/' \
                f'gpcc_media_anual_1981-2010.nc'

        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(gpcc.values, gpcc.coords['lat'].values, 
                   gpcc.coords['lon'].values, fname=ofile,
                   ntimes=gpcc.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


###### CRU ######

ifile = f'{script_dir}/../data/br-accum-mensal/' \
        f'cru_pre_mon_total_0.5x0.5_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

with xr.open_dataset(ifile) as dset:

    if compute_accum:

        cru = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                        .groupby('time.year') \
                        .sum('time', skipna=False)

        ofile = f'{script_dir}/../data/acumulado_anual/' \
                f'cru_acumulado_anual_1981-2010.nc'
        
        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(cru.values, cru.coords['lat'].values, 
                   cru.coords['lon'].values, fname=ofile,
                   ntimes=cru.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

    if compute_mean:

        cru = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                        .groupby('time.year') \
                        .mean('time', skipna=False)

        ofile = f'{script_dir}/../data/media_anual/' \
                f'cru_media_anual_1981-2010.nc'
        
        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(cru.values, cru.coords['lat'].values, 
                   cru.coords['lon'].values, fname=ofile,
                   ntimes=cru.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)



###### CPC ######

ifile = f'{script_dir}/../data/br-accum-mensal/' \
        f'cpc_precip_mon_total_0.5x0.5_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

with xr.open_dataset(ifile) as dset:

    if compute_accum:
 
        cpc = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                        .groupby('time.year') \
                        .sum('time', skipna=False)

        ofile = f'{script_dir}/../data/acumulado_anual/' \
                f'cpc_acumulado_anual_1981-2010.nc'
        
        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(cpc.values, cpc.coords['lat'].values, 
                   cpc.coords['lon'].values, fname=ofile,
                   ntimes=cpc.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

    if compute_mean:
 
        cpc = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                        .groupby('time.year') \
                        .mean('time', skipna=False)

        ofile = f'{script_dir}/../data/media_anual/' \
                f'cpc_media_anual_1981-2010.nc'
        
        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(cpc.values, cpc.coords['lat'].values, 
                   cpc.coords['lon'].values, fname=ofile,
                   ntimes=cpc.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


###### XAVIER ######

ifile = f'{script_dir}/../data/br-accum-mensal/' \
        f'xavier_prec_mon_total_0.25x0.25_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

with xr.open_dataset(ifile) as dset:

    if compute_accum:

        xavier = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                           .groupby('time.year') \
                           .sum('time', skipna=False)

        ofile = f'{script_dir}/../data/acumulado_anual/' \
                f'xavier_acumulado_anual_1981-2010.nc'
        
        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(xavier.values, xavier.coords['lat'].values, 
                   xavier.coords['lon'].values, fname=ofile,
                   ntimes=xavier.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

    if compute_mean:

        xavier = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                           .groupby('time.year') \
                           .mean('time', skipna=False)

        ofile = f'{script_dir}/../data/media_anual/' \
                f'xavier_media_anual_1981-2010.nc'
        
        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(xavier.values, xavier.coords['lat'].values, 
                   xavier.coords['lon'].values, fname=ofile,
                   ntimes=xavier.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


###### CHIRPS ######

ifile = f'{script_dir}/../data/br-accum-mensal/' \
        f'chirps_precip_mon_total_0.05x0.05_198101-201112_v2.0.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

with xr.open_dataset(ifile) as dset:

    if compute_accum:

        chirps = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                           .groupby('time.year') \
                           .sum('time', skipna=False)

        ofile = f'{script_dir}/../data/acumulado_anual/' \
                f'chirps_acumulado_anual_1981-2010.nc'
        
        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(chirps.values, chirps.coords['lat'].values, 
                   chirps.coords['lon'].values, fname=ofile,
                   ntimes=chirps.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)
 
    if compute_mean:

        chirps = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                           .groupby('time.year') \
                           .mean('time', skipna=False)

        ofile = f'{script_dir}/../data/media_anual/' \
                f'chirps_media_anual_1981-2010.nc'
        
        # a função substitui NaN por -999.0 que será o _Fillvalue
        writenc_yr(chirps.values, chirps.coords['lat'].values, 
                   chirps.coords['lon'].values, fname=ofile,
                   ntimes=chirps.values.shape[0])
        
        print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)
