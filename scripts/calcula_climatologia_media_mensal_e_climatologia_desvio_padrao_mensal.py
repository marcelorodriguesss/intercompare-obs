#!/usr/bin/env python3.6

import os
import xarray as xr
import numpy as np
from colorama import Fore, Back, Style

from writenc_clim_mon import writenc_clim_mon

compute_std = True
compute_mean = True

script_dir = os.path.dirname(os.path.realpath(__file__))

data_ini, data_fim = '1981-01', '2010-12'

##############################  CMAP  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
    f'cmap_precip_mon_total_2.5x2.5_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:

    with xr.open_dataset(ifile) as dset:

        cmap = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
        f'cmap_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(cmap.values, cmap.coords['lat'].values,
                     cmap.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:

    with xr.open_dataset(ifile) as dset:

        cmap = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
        f'cmap_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(cmap.values, cmap.coords['lat'].values,
                     cmap.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


##############################  GPCP  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
    f'gpcp_precip_mon_total_2.5x2.5_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:
    with xr.open_dataset(ifile) as dset:
        gpcp = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
        f'gpcp_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(gpcp.values, gpcp.coords['lat'].values,
                     gpcp.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:
    with xr.open_dataset(ifile) as dset:
        gpcp = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
        f'gpcp_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(gpcp.values, gpcp.coords['lat'].values,
                     gpcp.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


##############################  PRECL  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
    f'precl_precip_mon_total_0.5x0.5_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:
    with xr.open_dataset(ifile) as dset:
        precl = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
        f'precl_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(precl.values, precl.coords['lat'].values,
                     precl.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:
    with xr.open_dataset(ifile) as dset:
        precl = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
        f'precl_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(precl.values, precl.coords['lat'].values,
                     precl.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


##############################  DELAWARE  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
    f'delaware_precip_mon_total_0.5x0.5_198101-201112_v501.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:
    with xr.open_dataset(ifile) as dset:
        delaware = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
        f'delaware_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(delaware.values, delaware.coords['lat'].values,
                     delaware.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:
    with xr.open_dataset(ifile) as dset:
        delaware = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
        f'delaware_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(delaware.values, delaware.coords['lat'].values,
                     delaware.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


##############################  GPCC  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
    f'gpcc_precip_mon_total_0.5x0.5_198101-201112_v7.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:
    with xr.open_dataset(ifile) as dset:
        gpcc = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
        f'gpcc_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(gpcc.values, gpcc.coords['lat'].values,
                     gpcc.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:
    with xr.open_dataset(ifile) as dset:
        gpcc = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
        f'gpcc_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(gpcc.values, gpcc.coords['lat'].values,
                     gpcc.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


##############################  CRU  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
    f'cru_pre_mon_total_0.5x0.5_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:
    with xr.open_dataset(ifile) as dset:
        cru = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
        f'cru_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(cru.values, cru.coords['lat'].values,
                     cru.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:
    with xr.open_dataset(ifile) as dset:
        cru = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
        f'cru_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(cru.values, cru.coords['lat'].values,
                     cru.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


##############################  CPC  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
    f'cpc_precip_mon_total_0.5x0.5_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:
    with xr.open_dataset(ifile) as dset:
        cpc = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
        f'cpc_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(cpc.values, cpc.coords['lat'].values,
                     cpc.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:
    with xr.open_dataset(ifile) as dset:
        cpc = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
        f'cpc_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(cpc.values, cpc.coords['lat'].values,
                     cpc.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


##############################  XAVIER  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
    f'xavier_prec_mon_total_0.25x0.25_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:
    with xr.open_dataset(ifile) as dset:
        xavier = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
        f'xavier_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(xavier.values, xavier.coords['lat'].values,
                     xavier.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:
    with xr.open_dataset(ifile) as dset:
        xavier = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
        f'xavier_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(xavier.values, xavier.coords['lat'].values,
                     xavier.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


##############################  FUNCEME 0.5  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
        f'funceme_pr_mon_total_0.5x0.5_196101-201812.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:
    with xr.open_dataset(ifile) as dset:
        fun0p5 = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                 .groupby('time.month') \
                 .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
            f'funceme0p5_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(fun0p5.values, fun0p5.coords['lat'].values,
                     fun0p5.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:
    with xr.open_dataset(ifile) as dset:
        fun0p5 = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                 .groupby('time.month') \
                 .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
            f'funceme0p5_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(fun0p5.values, fun0p5.coords['lat'].values,
                     fun0p5.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


##############################  FUNCEME 0.25  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
        f'funceme_pr_mon_total_0.25x0.25_196101-201812.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:
    with xr.open_dataset(ifile) as dset:
        fun0p25 = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                  .groupby('time.month') \
                  .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
            f'funceme0p25_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(fun0p25.values, fun0p25.coords['lat'].values,
                     fun0p25.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:
    with xr.open_dataset(ifile) as dset:
        fun0p25 = dset['pr'].sel(time=slice(data_ini, data_fim)) \
                  .groupby('time.month') \
                  .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
            f'funceme0p25_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(fun0p25.values, fun0p25.coords['lat'].values,
                     fun0p25.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


##############################  SA24  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
        f'sa24_precip_mon_total_1x1_198101-201112.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:
    with xr.open_dataset(ifile) as dset:
        sa24 = dset['pr'].sel(time=slice(data_ini, data_fim)) \
               .groupby('time.month') \
               .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
            f'sa24_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(sa24.values, sa24.coords['lat'].values,
                     sa24.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:
    with xr.open_dataset(ifile) as dset:
        sa24 = dset['pr'].sel(time=slice(data_ini, data_fim)) \
               .groupby('time.month') \
               .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
            f'sa24_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(sa24.values, sa24.coords['lat'].values,
                     sa24.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)


##############################  CHIRPS  ##############################

ifile = f'{script_dir}/../data/acumulado_mensal/' \
    f'chirps_precip_mon_total_0.05x0.05_198101-201112_v2.0.nc'

print(Fore.YELLOW + f' input:  {ifile}' + Style.RESET_ALL)

if compute_mean:
    with xr.open_dataset(ifile) as dset:
        chirps = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .mean('time', skipna=False)

    ofile = f'{script_dir}/../data/climatologia_mensal/' \
        f'chirps_clim_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(chirps.values, chirps.coords['lat'].values,
                     chirps.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)

if compute_std:
    with xr.open_dataset(ifile) as dset:
        chirps = dset['pr'].sel(time=slice(data_ini, data_fim)) \
            .groupby('time.month') \
            .std('time', skipna=False)

    ofile = f'{script_dir}/../data/desvio_padrao_mensal/' \
            f'chirps_std_mensal_1981-2010.nc'

    # a função substitui NaN por -999.0 que será o _Fillvalue
    writenc_clim_mon(chirps.values, chirps.coords['lat'].values,
                     chirps.coords['lon'].values, fname=ofile)

    print(Fore.RED + f' output: {ofile}' + Style.RESET_ALL)
